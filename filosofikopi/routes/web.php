<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});
Route::get('/home', function () {return view('home');});
Route::get('/visit', function () {return view('visit');});
Route::get('/visitdetails', function () {return view('visitdetails');});
Route::get('/about', function () {return view('about');});
Route::get('/shop', function () {return view('shop');});
Route::get('/shopdetails', function () {return view('shopdetails');});
Route::get('/journal', function () {return view('journal');});
Route::get('/journaldetails', function () {return view('journaldetails');});
Route::get('/subscription', function () {return view('subscription');});
Route::get('/shipping', function () {return view('shipping');});
Route::get('/returns', function () {return view('returns');});
Route::get('/faq', function () {return view('faq');});
Route::get('/contact', function () {return view('contact');});
Route::get('/signin', function () {return view('signin');});
Route::get('/signup', function () {return view('signup');});
Route::get('/resetpassword', function () {return view('reset');});
Route::get('/cart', function () {return view('cart');});
Route::get('/payment', function () {return view('payment');});
Route::get('/checkout', function () {return view('checkout');});
Route::get('/person', function () {return view('person');});
Route::get('/personhistory', function () {return view('personhistory');});
Route::get('/personpayment', function () {return view('personpayment');});
Route::get('/personinvite', function () {return view('personinvite');});
Route::get('/lookbook', function () {return view('lookbook');});
Route::get('/howtopay', function () {return view('howtopay');});


/* For Admin*/
Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'namespace' => 'Admin'], function() {CRUD::resource('home', 'HomeCrudController');});
Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'namespace' => 'Admin'], function() {CRUD::resource('homeheader', 'HomeHeaderCrudController');});
Route::group(['prefix' => 'admin', 'middleware' => ['web', 'auth'], 'namespace' => 'Admin'], function () {CRUD::resource('menu-item', 'MenuItemCrudController');});