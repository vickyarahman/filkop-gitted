<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

    </head>
    <body>
    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">
        <!-- ==========================-->
        <!-- SEARCH MODAL-->
        <!-- ==========================-->
        <div class="header-search open-search">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                        <div class="navbar-search">
                            <form class="search-global">
                                <input type="text" placeholder="Type to search" autocomplete="off" name="s" value="" class="search-global__input"/>
                                <button class="search-global__btn"><i class="icon fa fa-search"></i></button>
                                <div class="search-global__note">Begin typing your search above and press return to search.</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <button type="button" class="search-close close"><i class="fa fa-times"></i></button>
        </div>
        <!-- ==========================-->
        <!-- MOBILE MENU-->
        <!-- ==========================-->
        <div data-off-canvas="mobile-slidebar left overlay" class="mobile-slidebar ">

            <ul class="nav navbar-nav">
                <li><a href="shop" >Shop</a></li>
                <li ><a href="visit" >Visit</a></li>
                <li><a href="about" >About</a></li>
                <li><a href="subscription">Subscription</a></li>
                <li><a href="journal">Journal</a></li>
            </ul>

        </div>

        <!-- ==========================-->
        <!-- POPAP MENU-->
        <!-- ==========================-->


        <div class="wrap-fixed-menu" id="fixedMenu" >
            <nav class="fullscreen-center-menu">

                <div class="menu-main-menu-container">

                    <ul class="nav navbar-nav">
                        <li><a href="shop" >Shop</a></li>
                        <li ><a href="visit" >Visit</a></li>
                        <li><a href="about" >About</a></li>
                        <li><a href="subscription">Subscription</a></li>
                        <li><a href="journal">Journal</a></li>
                    </ul>



                </div>    </nav>
            <button type="button" class="fullmenu-close"><i class="fa fa-times"></i></button>
        </div>

        <div data-canvas="container">
            <header class="header header-4 header-normal-width header-menu-middle navbar-fixed-top header-logo-black header-navbar-center header-navibox-1-right header-color-white" style="background-color: rgba(255, 255, 255, 0);">
                <div class="container container-boxed-width">
                    <div class="row" style="padding-top:25px;background-color: transparent">
                        <div class="col-sm-1 pull-left">
                            <div class="header-navibox-1-left hidden-xs"><a href="home-1.html"><i class="icon fa fa-user"></i> Sign up</a></div>
                            <div class="header-navibox-1 hidden-lg hidden-md hidden-sm">
                                <button class="js-toggle-screen toggle-menu-button  hidden-xs"><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i></button>
                            </div>
                        </div>
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="center-logo"><a href="home.html" class="navbar-brand scroll"><img src=" images/general/logo-font.png" alt="logo" class="normal-logo"/><img src=" media/general/logo-font-white.png" alt="logo" class="scroll-logo hidden-xs"/></a>
                                <button class="menu-mobile-button visible-xs-block js-toggle-mobile-slidebar toggle-menu-button"><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i></button>
                            </div>
                        </div>
                        <div class="col-sm-1 pull-right">
                            <div class="header-navibox-1">
                                <a href="#" class="btn_header_search"><i class="fa fa-search"></i></a>
                                <div class="header-cart"><a href="#"><i aria-hidden="true" class="fa fa-shopping-cart"></i></a><span class="header-cart-count bg-primary">3</span></div>
                            </div>
                        </div>
                    </div>
                    <nav id="nav" class="navbar" style="border-color: transparent">

                        <ul class="yamm main-menu nav navbar-nav hidden-xs">
                            <li class="dropdown"><a href="home" data-toggle="dropdown" class="dropdown-toggle">Shop
                                    <!-- Classic Dropdown--></a>
                                {{--<ul role="menu" class="dropdown-menu">--}}
                                    {{--<li><a href="home" class="fontapercued" >Home ver 01</a></li>--}}
                                    {{--<li><a href="home-2.html" >Home ver 02</a></li>--}}
                                {{--</ul>--}}
                            </li>
                            <li class="dropdown"><a href="visit" data-toggle="dropdown" class="dropdown-toggle">Visit
                                    <!-- Classic Dropdown--></a>

                            </li>
                            <li class="dropdown"><a href="about" data-toggle="dropdown" class="dropdown-toggle">About
                                    <!-- Classic Dropdown--></a>

                            </li>
                            <li class="dropdown"><a href="subscription" data-toggle="dropdown" class="dropdown-toggle">Subscription
                                    <!-- Classic Dropdown--></a>

                            </li>
                            <li class="dropdown"><a href="journal" data-toggle="dropdown" class="dropdown-toggle">Journal
                                    <!-- Classic Dropdown--></a>

                            </li>
                        </ul>
                    </nav>
                </div>
            </header>
            <!-- end .header-->
        </div>

        <div class="wrap-content" style="margin-top: -25%">
            <div id="main-slider" data-slider-width="1920px" data-slider-height="985px" data-slider-arrows="false" data-slider-buttons="false" class="main-slider slider-pro">
                <div class="sp-slides">
                    <!-- Slide 1-->
                    <div class="sp-slide"><img src="images/visits/visit1.jpg" alt="slider" class="sp-image"/>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1">
                                    <div data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="500" data-show-delay="400" data-hide-delay="400" class="main-slider__label sp-layer"> </div>
                                    <h2 data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="1200" data-show-delay="600" data-hide-delay="400" class="main-slider__title sp-layer fonthapercued" style="padding-top: 25%">Temukan dirimu disini</h2>
                                </div>
                                <button class="btn btn-primary btn-effect" style="margin-top:-5%;background-color: #0a0a0a;">S h o p</button>
                            </div>


                        </div>
                    </div>
                    <!-- Slide 2-->
                    <div class="sp-slide"><img src="images/visits/visit2.jpg" alt="slider" class="sp-image"/>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1">
                                    <div data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="500" data-show-delay="400" data-hide-delay="400" class="main-slider__label sp-layer"> </div>
                                    <h2 data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="1200" data-show-delay="600" data-hide-delay="400" class="main-slider__title sp-layer fonthecticed" style="padding-top: 25%">Temukan dirimu disini Lagi</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end main-slider-->

           
                <div class="col-md-12" style="text-align: center;margin-bottom: 20px;margin-top: 5%">
                    <h1 class="fgrey3 fonthapercued">SEE WHAT'S NEW</h1>
                    <i class="fa fa-2x fa-chevron-down color-fgrey5"></i>
                </div>

            <div class="section-area-home" style="margin-bottom: 5%">

                <div class="block-table block-table_padd_100">
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">

                                <a href="catalog-product.html"><img src="images/product/product1.jpg" /> </a>

                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">

                                <a href="catalog-product.html"><img src="images/product/product2.jpg" /></a>

                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">

                                <a href="catalog-product.html"><img src="images/product/product3.jpg" /></a>

                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                </div>

                <div class="block-table block-table_padd_100">
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory">
                            <section class="b-presentation b-presentation_sm">

                                <a href="catalog-product.html"><img src="images/product/product4.jpg" /> </a>

                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory">
                            <section class="b-presentation b-presentation_sm">

                                <a href="catalog-product.html"><img src="images/product/product5.jpg" /></a>

                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory">
                            <section class="b-presentation b-presentation_sm">

                                <a href="catalog-product.html"><img src="images/product/product6.jpg" /></a>

                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                </div>

                <div class="block-table block-table_padd_100">
                    <div class="block-table__cell col-md-4 newcategory-bordered">
                        <div class="block-table__inner bg-grey-2">
                            <section class="b-presentation b-presentation_sm">

                                <a href="catalog-product.html"><img src="images/product/product1.jpg" /> </a>

                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2">
                            <section class="b-presentation b-presentation_sm">

                                <a href="catalog-product.html"><img src="images/product/product2.jpg" /></a>

                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2">
                            <section class="b-presentation b-presentation_sm">

                                <a href="catalog-product.html"><img src="images/product/product3.jpg" /></a>

                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                </div>

            </div>



                    <div class="row">
                        <div class="col-xs-12 homevideo-container">
                            <iframe src="https://www.youtube.com/embed/D9d3KjChEi8?list=PLkwqKG1JvcShcvbXsf9GA5cY5L04bp0Zu" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>


            <div class="row">
                <div class="col-md-12" style="text-align: center;">
                    <h1 class="fgrey3 fonthapercued">SHOP NEW COLLECTION</h1>
                    <i class="fa fa-2x fa-chevron-down color-fgrey5"></i>
                </div>
            </div>

            <section class="section-area">
                    <div class="block-table__cell col-md-12 text-center">
                        <div class="block-table__inner bg-white">
                            <div class="main-flex-box fonttcopsed">
                                <div class="flex-box-content">
                                    <div data-min480="3" data-min768="3" data-min992="3" data-min1200="3" data-pagination="true" data-navigation="false" data-auto-play="4000" data-stop-on-hover="true" class="goods-carousel owl-carousel owl-theme enable-owl-carousel js-zoom-gallery">
                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/product/product5.jpg" class="b-goods__img js-zoom-images"><img src="images/product/product6.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">Mundilfari</h3>
                                                <div class="b-goods__price">$220.00</div>
                                                <div class="b-goods__label bg-primary fbgorange">50%</div>
                                            </div>
                                        </section>
                                        <!-- end b-goods-->

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/product/product6.jpg" class="b-goods__img js-zoom-images"><img src="images/product/product6.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">Mundilfari</h3>
                                                <div class="b-goods__price">$220.00</div>
                                                <div class="b-goods__label bg-primary">new</div>
                                            </div>
                                        </section>
                                        <!-- end b-goods-->

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/product/product7.jpg" class="b-goods__img js-zoom-images"><img src="images/product/product7.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">Pandora</h3>
                                                <div class="b-goods__price">$70.50</div>
                                            </div>
                                        </section>
                                        <!-- end b-goods-->

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/product/product8.jpg" class="b-goods__img js-zoom-images"><img src="images/product/product8.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">Mundilfari</h3>
                                                <div class="b-goods__price">$220.00</div>
                                            </div>
                                        </section>
                                        <!-- end b-goods-->

                                    </div>
                                    {{--<button class="btn btn-primary btn-effect fonthapercued">See &nbsp; More</button>--}}
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            <section class="section-area" style="margin-top: 5%;margin-bottom:5%;" >
                <div class="block-table__cell col-md-12 text-center" style="margin-top: -100px">
                    <div class="block-table__inner bg-white">
                        <div class="main-flex-box" style="background-color: #FF9944">
                            <div class="flex-box-content">
                                <div data-min480="1" data-min768="1" data-min992="1" data-min1200="1" data-pagination="true" data-navigation="false" data-auto-play="4000" data-stop-on-hover="true" class="goods-carousel owl-carousel owl-theme enable-owl-carousel js-zoom-gallery">
                                    <section class="b-goods">
                                        <h3 class="b-title-page__title shuffle fwhite">"Sesuatu dimulai dengan passion"</h3>
                                        <br>
                                        <br>
                                    </section>

                                    <section class="b-goods">
                                        <h3 class="b-title-page__title shuffle fwhite">"Sesuatu dimulai dengan passion"</h3>
                                        <br>
                                        <br>
                                    </section>

                                    <section class="b-goods">
                                        <h3 class="b-title-page__title shuffle fwhite">"Sesuatu dimulai dengan passion"</h3>
                                        <br>
                                        <br>
                                    </section>

                                    <section class="b-goods">
                                        <h3 class="b-title-page__title shuffle fwhite">"Sesuatu dimulai dengan passion"</h3>
                                        <br>
                                        <br>
                                        {{--<button class="btn btn-primary btn-effect fblack" style="background-color: #f0efef;">Get Direction</button>--}}
                                    </section>

                                    <section class="b-goods">
                                        <h3 class="b-title-page__title shuffle fwhite">"Sesuatu dimulai dengan passion"</h3>
                                        <br>
                                        <br>

                                    </section>

                                </div><br>
                                <button class="btn btn-primary btn-effect fblack" style="background-color: #f0efef;">See More</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            {{--<section class="section-area">--}}
                {{--<div class="row" style="width: 100%">--}}
                    {{--<div class="col-md-12 pull-left">--}}
                        {{--<h1 class="fontthecticed fgrey1 pull-left" style="padding-left: 50px"> Hey low</h1>--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</section>--}}

            <div class="b-form-newsletter bg-grey">

                <div class="row">
                    <div class="col-md-12">
                        <form class="b-form-newsletter__form col-md-4 fonttcopsed">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="b-form-newsletter__title pull-left">We're happy to help</div>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-md-6" style="text-align: left;">
                                    <div class="b-form-newsletter__title pull-left" style="font-size: 10pt">
                                        <i class="icon fa fa-clock-o" style="padding-right: 10px"></i>mon-sun 08.00 - 22.00
                                    </div>
                                </div>
                                <div class="col-md-6" style="text-align: left;">
                                    <div class="b-form-newsletter__title pull-left" style="font-size: 10pt">
                                        <i class="icon fa fa-phone" style="padding-right: 10px"></i>+62 5771 6802
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="text-align: left;">
                                    <div class="b-form-newsletter__title pull-left" style="font-size: 10pt">
                                        <i class="icon fa fa-envelope-o" style="padding-right: 10px"></i>hello@filosofikopi.com
                                    </div>
                                </div>
                                <div class="col-md-6" style="text-align: left;">
                                    <div class="b-form-newsletter__title pull-left" style="font-size: 10pt">
                                        <i class="icon fa fa-whatsapp" style="padding-right: 10px"></i>+62 5771 6802
                                    </div>
                                </div>
                            </div>
                        </form>

                        <form class="b-form-newsletter__form col-md-4 fonttcopsed">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="b-form-newsletter__title pull-left">To stay in the picture follow us</div>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-md-1">
                                    <div class="b-form-newsletter__title pull-left" style="font-size: 20pt">
                                        <i class="icon fa fa-facebook-f"></i>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="b-form-newsletter__title pull-left" style="font-size: 20pt">
                                        <i class="icon fa fa-instagram"></i>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="b-form-newsletter__title pull-left" style="font-size: 20pt">
                                        <i class="icon fa fa-twitter"></i>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="b-form-newsletter__title pull-left" style="font-size: 20pt">
                                        #filosofikopi
                                    </div>
                                </div>
                            </div>
                        </form>

                        <form class="b-form-newsletter__form col-md-4  fonttcopsed">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="b-form-newsletter__title pull-left">Sign up to our newsletter</div>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-md-8">
                                    <input style="color: #0a0a0a" type="email" placeholder="Enter your email" required="required" class="form-control"/>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn btn-primary btn-effect" style="background-color: #353535">Sign Up</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>



            <!-- end b-form-newsletter-->
            <footer class="footer bg-grey fonthapercued">
                <div class="content">

                        <div class="col-md-8 hidden-xs">
                            <h3 class="flfooter" style="text-align: left">
                                <a class="fgrey1" href="shop">Shop</a> &nbsp;&nbsp;&nbsp;
                                <a class="fgrey1" href="visit">Visit</a> &nbsp;&nbsp;&nbsp;
                                <a class="fgrey1" href="about">About</a> &nbsp;&nbsp;&nbsp;
                                <a class="fgrey1" href="journal">Journal</a> &nbsp;&nbsp;&nbsp;
                                <a class="fgrey1" href="shipping">Shipping</a> &nbsp;&nbsp;&nbsp;
                                <a class="fgrey1" href="returns">Returns</a> &nbsp;&nbsp;&nbsp;
                                <a class="fgrey1" href="faq">FAQ</a> &nbsp;&nbsp;&nbsp;
                                <a class="fgrey1" href="subscription">Subscription</a> &nbsp;&nbsp;&nbsp;
                                <a class="fgrey1" href="contact">Contact Us</a>
                            </h3>
                        </div>

                        <div class="col-xs-8 hidden-md hidden-lg">
                            <h3 class="flfootermobile col-xs-12" style="text-align: left">
                                <a class="fgrey1" href="shop">Shop</a>
                            </h3>
                            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                                <a class="fgrey1" href="visit">Visit</a>
                            </h3>
                            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                                <a class="fgrey1" href="about">About</a>
                            </h3>
                            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                                <a class="fgrey1" href="journal">Journal</a>
                            </h3>
                            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                                <a class="fgrey1" href="shipping">Shipping</a>
                            </h3>
                            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                                <a class="fgrey1" href="returns">Returns</a>
                            </h3>
                            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                                <a class="fgrey1" href="faq">FAQ</a>
                            </h3>
                            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                                <a class="fgrey1" href="subscription">Subscription</a>
                            </h3>
                            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                                <a class="fgrey1" href="contact">Contact Us</a>
                            </h3>
                        </div>
                        {{--<div class="col-sm-1">--}}
                            {{--<h3 class="flfooter" style="text-align: center">Visit</h3>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-1">--}}
                            {{--<h3 class="flfooter" style="text-align: center">About</h3>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-1">--}}
                            {{--<h3 class="flfooter" style="text-align: center">Journal</h3>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-1">--}}
                            {{--<h3 class="flfooter" style="text-align: center">Shipping</h3>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-1">--}}
                            {{--<h3 class="flfooter" style="text-align: center">Returns</h3>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-1">--}}
                            {{--<h3 class="flfooter" style="text-align: center">FAQ</h3>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-1">--}}
                            {{--<h3 class="flfooter" style="text-align: left">Subscription</h3>--}}
                        {{--</div>--}}

                        <div class="col-md-4">
                            <!-- end social-list-->
                            <div class="flcopyright" style="font-size: 14px;text-align: right">  2018 ® Filosofi Kopi </div>
                        </div>

                </div>
            </footer>
            <!-- end .footer-type-1-->


        </div>
        <!-- end layout-theme-->
    </div>


    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>
</html>
