<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

    </head>
    <body>
    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">

        @include('includes.headerblack')

        <div class="wrap-content" >

            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="section-policy">


                            <h3>Cara Pembayaran</h3>
                            <p>Saldo Anda dapat diisi ulang dengan pengisian tujuan kode bank DOKU (899) atau Bank Permata (013). Langkah pengisian melalui ATM BCA tujuan kode bank DOKU (899):</p>
    <br>
                            <h5>ATM</h5>
                            <p>Minimum transfer Rp 10.000,-</p>
                            <p>Pilih menu "Transfer"</p>
                            <p>Pilih menu "Ke Rek Bank Lain"</p>
                            <p>Masukkan sandi Bank "899" dan pilih "Benar"</p>
                            <p>Masukkan nominal isi ulang dan pilih "Benar"</p>
                            <p>Masukkan nomor rekening yang dituju dengan 10 digit DOKU ID Anda</p>
                            <p>Contoh : 1234567890 dan pilih "Benar"</p>
                            <p>Pilih "Benar" pada halaman konfirmasi</p>
    <br>
                            <h5>Mobile Banking</h5>
                            <p>Minimum transfer Rp 10.000,-</p>
                            <p>Pilih menu "m-Transfer"</p>
                            <p>Pilih menu "Transfer Antar Bank".</p>
                            <p>Masukkan "856666" + 10 digit DOKU ID Anda.</p>
                            <p>Contoh : 8566661234567890</p>
                            <p>Pilih "Bank Permata" pada Bank dan pilih tombol Lanjut.</p>
                            <p>Masukkan nominal isi ulang dan pilih tombol Lanjut</p>
                            <p>Transaksi ini akan dikenakan biaya sesuai dengan ketentuan tarif transfer antar bank. Setelah transaksi selesai, saldo DOKU Anda akan terisi secara otomatis dalam kurun waktu 5-10 menit.</p>

                        </section>
                    </div>
                </div>
            </div>

        </div>

    @include('includes.footer')
        <!-- end layout-theme-->
    </div>


    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>
</html>
