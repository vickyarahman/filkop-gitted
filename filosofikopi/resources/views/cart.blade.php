<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

    </head>
    <body>
    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">

        @include('includes.headerblack')

        <div class="wrap-content">

            <div class="container" style="padding-top: 5%;padding-bottom: 5%">
            <div class="row">
                <div class="col-md-9 ">

                    <div class="woocommerce">
                        <form action="http://pro-theme.com/wordpress/ismile/cart/" method="post" class="cart-table ">


                            <div class="b-table b-cart-table ">
                                <table class="shop_table shop_table_responsive cart table" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <td class="product-thumbnail">&nbsp;</td>
                                        <td class="product-name"><span>Product</span></td>
                                        <td class="product-price"><span>Price</span></td>
                                        <td class="product-quantity"><span>Quantity</span></td>
                                        <td class="product-subtotal"><span>Total</span></td>
                                        <td class="product-remove"><span>remove</span></td>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr class="cart_item">



                                        <td class="product-thumbnail">
                                            <a href="catalog-product.html">
                                                <img width="180" height="180" src="images/productdetails/main/product6.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image">
                                            </a>						</td>

                                        <td data-title="Product">
                                            <div class="caption">
                                                <a class="product-name" href="shopdetails">Filosofi Kopi Ride - Surga</a></div>
                                        </td>

                                        <td class="product-price" data-title="Price">
							<span class="product-price total-price">
							<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120,000</span>						    </span>
                                        </td>

                                        <td class="product-quantity" data-title="Quantity">

                                            <div class="input-group btn-block qty-block" data-trigger="spinner">


                                                <input data-rule="quantity" value="1" title="Qty" class="input-text qty text" >

                                            </div>


                                        </td>

                                        <td class="product-subtotal" data-title="Total">
                                            <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120,000</span>						</td>
                                        <td class="product-remove">

                                            <a href="shop" class="btn btn-remove" title="Remove this item" ><i class="fa fa-trash fa-lg"></i></a>						</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="actions">

                                            {{--<div class="coupon">--}}

                                                {{--<label for="coupon_code">Coupon:</label> <input type="text"  class="input-text"  placeholder="Coupon code"> <input type="submit" class="button" name="apply_coupon" value="Apply Coupon">--}}

                                            {{--</div>--}}


                                            <input type="hidden"  value="6bbd62fad3"><input type="hidden" >				</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>


                        </form>

                        <div class="cart-collaterals">

                            <div class="cart_totals calculated_shipping">


                                <h2>Cart Totals</h2>

                                <table cellspacing="0" class="shop_table shop_table_responsive">

                                    <tbody><tr class="cart-subtotal">
                                        <th>Subtotal</th>
                                        <td data-title="Subtotal"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120,000</span></td>
                                    </tr>






                                    <tr class="order-total">
                                        <th>Total</th>
                                        <td data-title="Total"><strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120,000</span></strong> </td>
                                    </tr>


                                    </tbody></table>

                                <div class="wc-proceed-to-checkout">

                                    <a href="checkout" class="checkout-button button alt wc-forward">
                                        Proceed to Checkout</a>
                                </div>


                            </div>

                        </div>

                    </div>




                </div>

                <div class="col-md-3">
                    <aside class="l-sidebar">
                        <form class="form-filter">

                            <section class="section-sidebar">
                                <h3 class="sidebar-title">categories</h3>
                                <div id="accordion" class="accordion accordion-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-1" class="btn-collapse"><i class="icon"></i> Men</a></div>
                                        <div id="categories-1" class="panel-collapse collapse in">
                                            <div class="label-group">
                                                <input id="categories__radio-1" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                <label for="categories__radio-1" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Apparel</a></label>
                                                <input id="categories__radio-2" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                <label for="categories__radio-2" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Hats</a></label>
                                                <input id="categories__radio-3" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                <label for="categories__radio-3" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Outer</a></label>
                                                <input id="categories__radio-4" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                <label for="categories__radio-4" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Bag</a></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-2" class="btn-collapse"><i class="icon"></i> Women</a></div>
                                        <div id="categories-2" class="panel-collapse collapse">
                                            <div class="label-group">
                                                <input id="categories__radio-5" type="radio" name="categories-group-2" value="" class="forms__radio hidden"/>
                                                <label for="categories__radio-5" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Apparel</a></label>
                                                <input id="categories__radio-6" type="radio" name="categories-group-2" value="" class="forms__radio hidden"/>
                                                <label for="categories__radio-6" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Hats</a></label>
                                                <input id="categories__radio-7" type="radio" name="categories-group-2" value="" class="forms__radio hidden"/>
                                                <label for="categories__radio-7" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Outer</a></label>
                                                <input id="categories__radio-8" type="radio" name="categories-group-2" value="" class="forms__radio hidden"/>
                                                <label for="categories__radio-8" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Bag</a></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-3" class="btn-collapse"><i class="icon"></i> Coffee</a></div>
                                        <div id="categories-3" class="panel-collapse collapse">
                                            <div class="label-group">
                                                <input id="categories__radio-5" type="radio" name="categories-group-3" value="" class="forms__radio hidden"/>
                                                <label for="categories__radio-5" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Coffee</a></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-4" class="btn-collapse"><i class="icon"></i> Equipment</a></div>
                                        <div id="categories-4" class="panel-collapse collapse">
                                            <div class="label-group">
                                                <label for="categories__radio-5" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Manual Brewing</a></label>
                                                <input id="categories__radio-5" type="radio" name="categories-group-4" value="" class="forms__radio hidden"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-5" class="btn-collapse"><i class="icon"></i> Sales</a></div>
                                        <div id="categories-5" class="panel-collapse collapse">
                                            <label for="categories__radio-5" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Filosofi Kopi - Ride</a></label>
                                            <input id="categories__radio-5" type="radio" name="categories-group-5" value="" class="forms__radio hidden"/>
                                        </div>
                                    </div>
                                </div>
                            </section>

                        </form>
                    </aside>
                    <!-- end .sidebar-->


                </div>

            </div>
            </div>

        </div>


        <!-- end layout-theme-->
        @include('includes.footer')
    </div>



    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>
</html>
