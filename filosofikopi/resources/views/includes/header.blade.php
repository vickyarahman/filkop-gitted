<!-- ==========================-->
<!-- SEARCH MODAL-->
<!-- ==========================-->
<div class="header-search open-search">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <div class="navbar-search">
                    <form class="search-global">
                        <input type="text" placeholder="Type to search" autocomplete="off" name="s" value="" class="search-global__input"/>
                        <button class="search-global__btn"><i class="icon fa fa-search"></i></button>
                        <div class="search-global__note">Begin typing your search above and press return to search.</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="search-close close"><i class="fa fa-times"></i></button>
</div>
<!-- ==========================-->
<!-- MOBILE MENU-->
<!-- ==========================-->
<div data-off-canvas="mobile-slidebar left overlay" class="mobile-slidebar ">

    <ul class="nav navbar-nav">
        <li><a href="shop" >Shop</a></li>
        <li ><a href="visit" >Visit</a></li>
        <li><a href="about" >About</a></li>
        <li><a href="subscription">Subscription</a></li>
        <li><a href="journal">Journal</a></li>
    </ul>

</div>

<!-- ==========================-->
<!-- POPAP MENU-->
<!-- ==========================-->


<div class="wrap-fixed-menu" id="fixedMenu" >
    <nav class="fullscreen-center-menu">

        <div class="menu-main-menu-container">

            <ul class="nav navbar-nav">
                <li><a href="shop" >Shop</a></li>
                <li ><a href="visit" >Visit</a></li>
                <li><a href="about" >About</a></li>
                <li><a href="subscription">Subscription</a></li>
                <li><a href="journal">Journal</a></li>
            </ul>



        </div>    </nav>
    <button type="button" class="fullmenu-close"><i class="fa fa-times"></i></button>
</div>

<div data-canvas="container">
    <header class="header header-4 header-normal-width header-menu-middle navbar-fixed-top header-logo-black header-navbar-center header-navibox-1-right header-color-white" style="background-color: rgba(255, 255, 255, 0);">
        <div class="container container-boxed-width fonttapercued">
            <div class="row" style="padding-top:25px;background-color: transparent">
                <div class="col-sm-1 pull-left">
                    <div class="header-navibox-1-left hidden-xs"><a href="home"><i class="icon fa fa-user"></i> Sign up</a></div>
                    <div class="header-navibox-1 hidden-lg hidden-md hidden-sm">
                        <button class="js-toggle-screen toggle-menu-button  hidden-xs"><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i></button>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="center-logo"><a href="home" class="navbar-brand scroll"><img src=" images/general/logo-font.png" alt="logo" class="normal-logo"/><img src=" media/general/logo-font-white.png" alt="logo" class="scroll-logo hidden-xs"/></a>
                        <button class="menu-mobile-button visible-xs-block js-toggle-mobile-slidebar toggle-menu-button"><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i></button>
                    </div>
                </div>
                <div class="col-sm-1 pull-right">
                    <div class="header-navibox-1">
                        <a href="#" class="btn_header_search"><i class="fa fa-search"></i></a>
                        <div class="header-cart"><a href="cart"><i aria-hidden="true" class="fa fa-shopping-cart"></i></a><span class="header-cart-count bg-primary">1</span></div>
                    </div>
                </div>
            </div>
            <nav id="nav" class="navbar" style="border-color: transparent">

                <ul class="yamm main-menu nav navbar-nav hidden-xs">
                    <li class="dropdown"><a href="shop" data-toggle="dropdown" class="dropdown-toggle">Shop
                            <!-- Classic Dropdown--></a>
                        {{--<ul role="menu" class="dropdown-menu">--}}
                        {{--<li><a href="home" class="fontapercued" >Home ver 01</a></li>--}}
                        {{--<li><a href="home-2.html" >Home ver 02</a></li>--}}
                        {{--</ul>--}}
                    </li>
                    <li class="dropdown"><a href="visit" data-toggle="dropdown" class="dropdown-toggle">Visit
                            <!-- Classic Dropdown--></a>

                    </li>
                    <li class="dropdown"><a href="about" data-toggle="dropdown" class="dropdown-toggle">About
                            <!-- Classic Dropdown--></a>

                    </li>
                    <li class="dropdown"><a href="faq" data-toggle="dropdown" class="dropdown-toggle">Subscription
                            <!-- Classic Dropdown--></a>

                    </li>
                    <li class="dropdown"><a href="faq" data-toggle="dropdown" class="dropdown-toggle">Journal
                            <!-- Classic Dropdown--></a>

                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <!-- end .header-->
</div>