<div class="b-form-newsletter bg-grey">

    <div class="row">
        <div class="col-md-12">
            <form class="b-form-newsletter__form col-md-4 fonttcopsed">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="b-form-newsletter__title pull-left">We're happy to help</div>
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-md-6" style="text-align: left;">
                        <div class="b-form-newsletter__title pull-left" style="font-size: 10pt">
                            <i class="icon fa fa-clock-o" style="padding-right: 10px"></i>mon-sun 08.00 - 22.00
                        </div>
                    </div>
                    <div class="col-md-6" style="text-align: left;">
                        <div class="b-form-newsletter__title pull-left" style="font-size: 10pt">
                            <i class="icon fa fa-phone" style="padding-right: 10px"></i>+62 5771 6802
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="text-align: left;">
                        <div class="b-form-newsletter__title pull-left" style="font-size: 10pt">
                            <i class="icon fa fa-envelope-o" style="padding-right: 10px"></i>hello@filosofikopi.com
                        </div>
                    </div>
                    <div class="col-md-6" style="text-align: left;">
                        <div class="b-form-newsletter__title pull-left" style="font-size: 10pt">
                            <i class="icon fa fa-whatsapp" style="padding-right: 10px"></i>+62 5771 6802
                        </div>
                    </div>
                </div>
            </form>

            <form class="b-form-newsletter__form col-md-4 fonttcopsed">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="b-form-newsletter__title pull-left">To stay in the picture follow us</div>
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-md-1">
                        <div class="b-form-newsletter__title pull-left" style="font-size: 20pt">
                            <i class="icon fa fa-facebook-f"></i>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="b-form-newsletter__title pull-left" style="font-size: 20pt">
                            <i class="icon fa fa-instagram"></i>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="b-form-newsletter__title pull-left" style="font-size: 20pt">
                            <i class="icon fa fa-twitter"></i>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="b-form-newsletter__title pull-left" style="font-size: 20pt">
                            #filosofikopi
                        </div>
                    </div>
                </div>
            </form>

            <form class="b-form-newsletter__form col-md-4  fonttcopsed">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="b-form-newsletter__title pull-left">Sign up to our newsletter</div>
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-md-8">
                        <input style="color: #0a0a0a" type="email" placeholder="Enter your email" required="required" class="form-control"/>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-primary btn-effect" style="background-color: #353535">Sign Up</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>



<!-- end b-form-newsletter-->
<footer class="footer bg-grey fonthapercued">
    <div class="content">

        <div class="col-md-8 hidden-xs">
            <h3 class="flfooter" style="text-align: left">
                <a class="fgrey1" href="shop">Shop</a> &nbsp;&nbsp;&nbsp;
                <a class="fgrey1" href="visit">Visit</a> &nbsp;&nbsp;&nbsp;
                <a class="fgrey1" href="about">About</a> &nbsp;&nbsp;&nbsp;
                <a class="fgrey1" href="journal">Journal</a> &nbsp;&nbsp;&nbsp;
                <a class="fgrey1" href="shipping">Shipping</a> &nbsp;&nbsp;&nbsp;
                <a class="fgrey1" href="returns">Returns</a> &nbsp;&nbsp;&nbsp;
                <a class="fgrey1" href="faq">FAQ</a> &nbsp;&nbsp;&nbsp;
                <a class="fgrey1" href="subscription">Subscription</a> &nbsp;&nbsp;&nbsp;
                <a class="fgrey1" href="contact">Contact Us</a>
            </h3>
        </div>

        <div class="col-xs-8 hidden-md hidden-lg">
            <h3 class="flfootermobile col-xs-12" style="text-align: left">
                <a class="fgrey1" href="shop">Shop</a>
            </h3>
            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                <a class="fgrey1" href="visit">Visit</a>
            </h3>
            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                <a class="fgrey1" href="about">About</a>
            </h3>
            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                <a class="fgrey1" href="journal">Journal</a>
            </h3>
            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                <a class="fgrey1" href="shipping">Shipping</a>
            </h3>
            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                <a class="fgrey1" href="returns">Returns</a>
            </h3>
            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                <a class="fgrey1" href="faq">FAQ</a>
            </h3>
            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                <a class="fgrey1" href="subscription">Subscription</a>
            </h3>
            <h3 class="flfootermobile col-xs-12 fgrey1" style="text-align: left">
                <a class="fgrey1" href="contact">Contact Us</a>
            </h3>
        </div>
        {{--<div class="col-sm-1">--}}
        {{--<h3 class="flfooter" style="text-align: center">Visit</h3>--}}
        {{--</div>--}}
        {{--<div class="col-sm-1">--}}
        {{--<h3 class="flfooter" style="text-align: center">About</h3>--}}
        {{--</div>--}}
        {{--<div class="col-sm-1">--}}
        {{--<h3 class="flfooter" style="text-align: center">Journal</h3>--}}
        {{--</div>--}}
        {{--<div class="col-sm-1">--}}
        {{--<h3 class="flfooter" style="text-align: center">Shipping</h3>--}}
        {{--</div>--}}
        {{--<div class="col-sm-1">--}}
        {{--<h3 class="flfooter" style="text-align: center">Returns</h3>--}}
        {{--</div>--}}
        {{--<div class="col-sm-1">--}}
        {{--<h3 class="flfooter" style="text-align: center">FAQ</h3>--}}
        {{--</div>--}}
        {{--<div class="col-sm-1">--}}
        {{--<h3 class="flfooter" style="text-align: left">Subscription</h3>--}}
        {{--</div>--}}

        <div class="col-md-4">
            <!-- end social-list-->
            <div class="flcopyright" style="font-size: 14px;text-align: right">  2018 ® Filosofi Kopi </div>
        </div>

    </div>
</footer>
<!-- end .footer-type-1-->