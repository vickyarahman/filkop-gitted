<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

        <script  type='text/javascript'>
            $(document).ready(function(){
                $(".productichi").hover(
                    function() {$(this).attr("src","images/product/product2.jpg");},
                    function() {$(this).attr("src","images/product/product3.jpg");
                    });
            });
        </script>

    </head>
    <body>
    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">

        @include('includes.headerwhite')

        <div class="wrap-content">

            {{--Navigation Breadcrumb--}}
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            <li class="fblack"><a href="Home">Home</a></li>
                            <li class="active fblack">Shop</li>
                        </ol>
                    </div>
                </div>
            </div>
            {{--End Navigation Breadcrumb--}}

            {{--Start Head Shop Category--}}

            <div class="section-area" style="margin-bottom: 5%">
                <div class="block-table block-table_padd_0">
                    <div class="block-table__cell col-md-3">
                        <div class="block-table__inner fbgorange">
                            <section class="b-presentation b-presentation_sm">

                                    <a href="#">

                                        {{--<img src="images/barista/cel.png" alt="goods" class="b-presentation__img scrollreveal" style="background-color: #fd7e14;"/></a>--}}
                                    <h3 class="fwhite fonthapercued" style="line-height: 225px;text-align: center;" > Sidi sidian</h3>
                                    </a>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>

                    <div class="block-table__cell col-md-3">
                        <div class="block-table__inner bg-grey-2">
                            <section class="b-presentation b-presentation_sm">

                                <a href="#"><img style="min-height: 225px;object-fit: fill" src="images/product/product4.jpg" alt="goods" class="b-presentation__img scrollreveal"/></a>
                                <h3 class="timcentered fonthapercued fbgorange fwhite" style="text-align: center;padding: 5px" > Celi celian</h3>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>

                    <div class="block-table__cell col-md-3">
                        <div class="block-table__inner fbgorange">
                            <section class="b-presentation b-presentation_sm">

                                <a href="#">

                                    {{--<img src="images/barista/cel.png" alt="goods" class="b-presentation__img scrollreveal" style="background-color: #fd7e14;"/></a>--}}
                                    <h3 class="fwhite fonthapercued" style="line-height: 225px;text-align: center;" > Equipment</h3>
                                </a>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>

                    <div class="block-table__cell col-md-3">
                        <div class="block-table__inner bg-grey-2">
                            <section class="b-presentation b-presentation_sm">

                                <a href="#"><img style="object-fit: cover" src="images/product/product2.jpg" alt="goods" class="b-presentation__img scrollreveal"/></a>
                                <h3 class="timcentered fonthapercued fbgorange fwhite" style="text-align: center;padding: 5px" > Tote</h3>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                </div>
            </div>

            {{--End Head Shop Category--}}

            {{--Start Shop Content--}}
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="b-goods-number-products">Showing 1 to 10 of 9 products</div>
                    </div>
                    <div class="col-md-9">
                        <div class="b-goods-headers">
                            <div class="b-goods-headers__view"><i class="b-goods-headers__view-item active fa fa-th js-view-col"></i><i class="b-goods-headers__view-item fa fa-list js-view-list"></i></div>
                            <div class="b-goods-headers__sorting">
                                <div class="b-goods-headers__sorting-name">Sort by:</div>
                                <select data-width="auto" class="selectpicker">
                                    <option>Popularity</option>
                                    <option>Popularity 1</option>
                                    <option>Popularity 2</option>
                                    <option>Popularity 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-9 col-md-push-3">
                        <div class="l-main-content">
                            <div class="b-goods-catalog">

                                <!-- Product 1 -->
                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><a href="images/product/product7.jpg" class="b-goods__img js-zoom-images"><img src="images/product/product7.jpg" alt="goods" class="productionichi"/></a>
                                        <div class="b-goods__wrap">
                                             <div class="b-goods__category">
                                                 <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                             </div><br>
                                            <h3 class="b-goods__name">Sidijelek</h3>
                                            <div class="b-goods__description">"ngawur" </div>
                                            <div class="b-goods__price">Rp 100,000</div>
                                        </div>
                                    </div>
                                </section>
                                <!-- end b-goods-->


                                <!-- Product 2 -->
                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><a href="images/product/product5.jpg" class="b-goods__img js-zoom-images"><img src="images/product/product1.jpg" alt="goods" class="productionichi"/></a>
                                        <div class="b-goods__wrap">
                                            <div class="b-goods__category">
                                                <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                            </div><br>
                                            <h3 class="b-goods__name">FRD01</h3>
                                            <div class="b-goods__description">"Young soul never get old". Combed 30's. Size on demand. Grab it now!. </div>
                                            <div class="b-goods__price">Rp 160,000</div>
                                        </div>
                                    </div>
                                </section>
                                <!-- end b-goods-->

                                {{--<section class="b-goods b-goods_mod-a b-goods_3-col">--}}
                                    {{--<div class="b-goods__inner"><a href="images/product/homemarket2.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket2.jpg" alt="goods" class="img-responsive"/></a>--}}
                                        {{--<div class="b-goods__wrap">--}}
                                            {{--<div class="b-goods__category">--}}
                                                 {{--<a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>--}}
                                            {{--</div><br>--}}
                                            {{--<h3 class="b-goods__name">FxM01</h3>--}}
                                            {{--<div class="b-goods__description">"Stranger can be mysterious, yet compelling". Combed 30's. Size on demand. Grab it now!. </div>--}}
                                            {{--<div class="b-goods__price">Rp 90,000</div>--}}
                                            {{--<div class="b-goods__price-old">Rp 160,000</div>--}}
                                            {{--<div class="b-goods__label bg-secondary">Sale</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</section>--}}
                                {{--<!-- end b-goods-->--}}

                                {{--<section class="b-goods b-goods_mod-a b-goods_3-col">--}}
                                    {{--<div class="b-goods__inner"><a href="images/product/homemarket3.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket3.jpg" alt="goods" class="img-responsive"/></a>--}}
                                        {{--<div class="b-goods__wrap">--}}
                                             {{--<div class="b-goods__category">--}}
                                                 {{--<a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>--}}
                                             {{--</div><br>--}}
                                            {{--<h3 class="b-goods__name">FxM02</h3>--}}
                                            {{--<div class="b-goods__description">"Get struck by the light and DOPE". Combed 30's. Size on demand. Grab it now!. </div>--}}
                                            {{--<div class="b-goods__price">Rp 160,000</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</section>--}}
                                {{--<!-- end b-goods-->--}}

                                {{--<section class="b-goods b-goods_mod-a b-goods_3-col">--}}
                                    {{--<div class="b-goods__inner"><a href="images/product/homemarket4.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket4.jpg" alt="goods" class="img-responsive"/></a>--}}
                                        {{--<div class="b-goods__wrap">--}}
                                             {{--<div class="b-goods__category">--}}
                                                 {{--<a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>--}}
                                             {{--</div><br>--}}
                                            {{--<h3 class="b-goods__name">FxM03</h3>--}}
                                            {{--<div class="b-goods__description">"Young soul never get old". Combed 30's. Size on demand. Grab it now!. </div>--}}
                                            {{--<div class="b-goods__price">Rp 160,000</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</section>--}}
                                {{--<!-- end b-goods-->--}}

                                {{--<section class="b-goods b-goods_mod-a b-goods_3-col">--}}
                                    {{--<div class="b-goods__inner"><a href="images/product/homemarket5.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket5.jpg" alt="goods" class="img-responsive"/></a>--}}
                                        {{--<div class="b-goods__wrap">--}}
                                             {{--<div class="b-goods__category">--}}
                                                 {{--<a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>--}}
                                             {{--</div><br>--}}
                                            {{--<h3 class="b-goods__name">FxM04</h3>--}}
                                            {{--<div class="b-goods__description">"Young soul never get old". Combed 30's. Size on demand. Grab it now!. </div>--}}
                                            {{--<div class="b-goods__price">Rp 160,000</div>--}}
                                            {{--<div class="b-goods-links"><a href="home.html" class="b-goods-links__item"><i class="icon fa fa-random"></i></a><a href="home.html" class="b-goods-links__item b-goods-links__item_main">See More </a><br><a href="home.html" class="b-goods-links__item"><i class="icon fa fa-heart-o"></i></a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</section>--}}
                                {{--<!-- end b-goods-->--}}

                                {{--<section class="b-goods b-goods_mod-a b-goods_3-col">--}}
                                    {{--<div class="b-goods__inner"><a href="images/product/homemarket6.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket6.jpg" alt="goods" class="img-responsive"/></a>--}}
                                        {{--<div class="b-goods__wrap">--}}
                                             {{--<div class="b-goods__category">--}}
                                                 {{--<a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>--}}
                                             {{--</div><br>--}}
                                            {{--<h3 class="b-goods__name">FxM05</h3>--}}
                                            {{--<div class="b-goods__description">"Young soul never get old". Combed 30's. Size on demand. Grab it now!. </div>--}}
                                            {{--<div class="b-goods__price">Rp 160,000</div>--}}
                                            {{--<div class="b-goods-links"><a href="home.html" class="b-goods-links__item"><i class="icon fa fa-random"></i></a><a href="home.html" class="b-goods-links__item b-goods-links__item_main">See More </a><br><a href="home.html" class="b-goods-links__item"><i class="icon fa fa-heart-o"></i></a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</section>--}}
                                {{--<!-- end b-goods-->--}}

                                {{--<section class="b-goods b-goods_mod-a b-goods_3-col">--}}
                                    {{--<div class="b-goods__inner"><a href="images/product/homemarket7.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket7.jpg" alt="goods" class="img-responsive"/></a>--}}
                                        {{--<div class="b-goods__wrap">--}}
                                             {{--<div class="b-goods__category">--}}
                                                 {{--<a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>--}}
                                             {{--</div><br>--}}
                                            {{--<h3 class="b-goods__name">FxM06</h3>--}}
                                            {{--<div class="b-goods__description">"Young soul never get old". Combed 30's. Size on demand. Grab it now!. </div>--}}
                                            {{--<div class="b-goods__price">Rp 160,000</div>--}}
                                            {{--<div class="b-goods-links"><a href="home.html" class="b-goods-links__item"><i class="icon fa fa-random"></i></a><a href="home.html" class="b-goods-links__item b-goods-links__item_main">See More </a><br><a href="home.html" class="b-goods-links__item"><i class="icon fa fa-heart-o"></i></a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</section>--}}
                                {{--<!-- end b-goods-->--}}

                                {{--<section class="b-goods b-goods_mod-a b-goods_3-col">--}}
                                    {{--<div class="b-goods__inner"><a href="images/product/homemarket8.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket8.jpg" alt="goods" class="img-responsive"/></a>--}}
                                        {{--<div class="b-goods__wrap">--}}
                                             {{--<div class="b-goods__category">--}}
                                                 {{--<a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a></div><br>--}}
                                            {{--<h3 class="b-goods__name">FxM07</h3>--}}
                                            {{--<div class="b-goods__description">"Young soul never get old". Combed 30's. Size on demand. Grab it now!. </div>--}}
                                            {{--<div class="b-goods__price">Rp 160,000</div>--}}
                                            {{--<div class="b-goods-links"><a href="home.html" class="b-goods-links__item"><i class="icon fa fa-random"></i></a><a href="home.html" class="b-goods-links__item b-goods-links__item_main">See More </a><br><a href="home.html" class="b-goods-links__item"><i class="icon fa fa-heart-o"></i></a></div>--}}
                                            {{--<div class="b-goods__label bg-secondary">sale</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</section>--}}
                                {{--<!-- end b-goods-->--}}

                                {{--<section class="b-goods b-goods_mod-a b-goods_3-col">--}}
                                    {{--<div class="b-goods__inner"><a href="images/product/homemarket9.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket9.jpg" alt="goods" class="img-responsive"/></a>--}}
                                        {{--<div class="b-goods__wrap">--}}
                                             {{--<div class="b-goods__category">--}}
                                                 {{--<a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>--}}
                                             {{--</div><br>--}}
                                            {{--<h3 class="b-goods__name">FxM08</h3>--}}
                                            {{--<div class="b-goods__description">"Young soul never get old". Combed 30's. Size on demand. Grab it now!. </div>--}}
                                            {{--<div class="b-goods__price">Rp 160,000</div>--}}
                                            {{--<div class="b-goods-links"><a href="home.html" class="b-goods-links__item"><i class="icon fa fa-random"></i></a><a href="home.html" class="b-goods-links__item b-goods-links__item_main">See More </a><br><a href="home.html" class="b-goods-links__item"><i class="icon fa fa-heart-o"></i></a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</section>--}}
                                {{--<!-- end b-goods-->--}}

                            </div>
                            <div class="text-right">
                                <ul class="pagination">
                                    <li class="active"><a href="#">1</a></li>
                                    {{--<li><a href="#">2</a></li>--}}
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-pull-9">
                        <aside class="l-sidebar">
                            <form class="form-filter">

                                {{--Sidebar category shop by--}}
                                <section class="section-sidebar">
                                    <h3 class="sidebar-title">Shop by</h3>
                                    <div id="accordion" class="accordion accordion-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-1" class="btn-collapse"><i class="icon"></i> Men</a></div>
                                            <div id="categories-1" class="panel-collapse collapse in">
                                                <div class="label-group">
                                                    <input id="categories__radio-1" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-1" class="forms__label forms__label-radio forms__label-radio-1">Apparel</label>
                                                    <input id="categories__radio-2" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-2" class="forms__label forms__label-radio forms__label-radio-1">Hats</label>
                                                    <input id="categories__radio-3" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-3" class="forms__label forms__label-radio forms__label-radio-1">Outer</label>
                                                    <input id="categories__radio-4" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-4" class="forms__label forms__label-radio forms__label-radio-1">Bag</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-2" class="btn-collapse"><i class="icon"></i> Women</a></div>
                                            <div id="categories-2" class="panel-collapse collapse">
                                                <div class="label-group">
                                                    <input id="categories__radio-5" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-5" class="forms__label forms__label-radio forms__label-radio-1">Apparel</label>
                                                    <input id="categories__radio-6" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-6" class="forms__label forms__label-radio forms__label-radio-1">Hats</label>
                                                    <input id="categories__radio-7" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-7" class="forms__label forms__label-radio forms__label-radio-1">Outer</label>
                                                    <input id="categories__radio-8" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-8" class="forms__label forms__label-radio forms__label-radio-1">Bag</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-3" class="btn-collapse"><i class="icon"></i> Coffee</a></div>
                                            <div id="categories-3" class="panel-collapse collapse">
                                                <div class="label-group">
                                                    {{--<input id="categories__radio-10" type="radio" name="categories-group-3" value="" checked="checked" class="forms__radio hidden"/>--}}
                                                    {{--<label for="categories__radio-10" class="forms__label forms__label-radio forms__label-radio-1">Accessories 1</label>--}}
                                                    {{--<input id="categories__radio-11" type="radio" name="categories-group-3" value="" class="forms__radio hidden"/>--}}
                                                    {{--<label for="categories__radio-11" class="forms__label forms__label-radio forms__label-radio-1">Accessories 2</label>--}}
                                                    {{--<input id="categories__radio-12" type="radio" name="categories-group-3" value="" class="forms__radio hidden"/>--}}
                                                    {{--<label for="categories__radio-12" class="forms__label forms__label-radio forms__label-radio-1">Accessories 3</label>--}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-4" class="btn-collapse"><i class="icon"></i> Equipment</a></div>
                                            <div id="categories-4" class="panel-collapse collapse">
                                                <div class="label-group">
                                                    {{--<input id="categories__radio-13" type="radio" name="categories-group-4" value="" checked="checked" class="forms__radio hidden"/>--}}
                                                    {{--<label for="categories__radio-13" class="forms__label forms__label-radio forms__label-radio-1">Bags 1</label>--}}
                                                    {{--<input id="categories__radio-14" type="radio" name="categories-group-4" value="" class="forms__radio hidden"/>--}}
                                                    {{--<label for="categories__radio-14" class="forms__label forms__label-radio forms__label-radio-1">Bags 2</label>--}}
                                                    {{--<input id="categories__radio-15" type="radio" name="categories-group-4" value="" class="forms__radio hidden"/>--}}
                                                    {{--<label for="categories__radio-15" class="forms__label forms__label-radio forms__label-radio-1">Bags 3</label>--}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-5" class="btn-collapse"><i class="icon"></i> Sales</a></div>
                                            <div id="categories-5" class="panel-collapse collapse">
                                                <div class="label-group">
                                                    {{--<input id="categories__radio-16" type="radio" name="categories-group-5" value="" checked="checked" class="forms__radio hidden"/>--}}
                                                    {{--<label for="categories__radio-16" class="forms__label forms__label-radio forms__label-radio-1">products 1</label>--}}
                                                    {{--<input id="categories__radio-17" type="radio" name="categories-group-5" value="" class="forms__radio hidden"/>--}}
                                                    {{--<label for="categories__radio-17" class="forms__label forms__label-radio forms__label-radio-1">products 2</label>--}}
                                                    {{--<input id="categories__radio-18" type="radio" name="categories-group-5" value="" class="forms__radio hidden"/>--}}
                                                    {{--<label for="categories__radio-18" class="forms__label forms__label-radio forms__label-radio-1">products 3</label>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                {{--End sidebar category shop by--}}

                                {{--Sidebar category refine by--}}
                                <section class="section-sidebar">
                                    <h3 class="sidebar-title">Refine by</h3>
                                    <div id="accordion" class="accordion accordion-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-6" class="btn-collapse"><i class="icon"></i> Color</a></div>
                                            <div id="categories-6" class="panel-collapse collapse">
                                                <div class="label-group">
                                                    <div class="checkbox-group">
                                                        <input id="check-1" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-1" class="forms__label forms__label-check forms__label-check-2 fblack">Black</label>
                                                        <input id="check-2" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-2" class="forms__label forms__label-check forms__label-check-2 fgrey8">White</label>
                                                        <input id="check-3" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-3" class="forms__label forms__label-check forms__label-check-2 forange">Blue</label>
                                                        <input id="check-4" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-4" class="forms__label forms__label-check forms__label-check-2">Green</label>
                                                        <input id="check-5" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-5" class="forms__label forms__label-check forms__label-check-2">Yellow</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-7" class="btn-collapse"><i class="icon"></i> Type</a></div>
                                            <div id="categories-7" class="panel-collapse collapse">
                                                <div class="label-group">
                                                    <div class="checkbox-group">
                                                        <input id="check-1" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-1" class="forms__label forms__label-check forms__label-check-2 fblack">Black</label>
                                                        <input id="check-2" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-2" class="forms__label forms__label-check forms__label-check-2 fgrey8">White</label>
                                                        <input id="check-3" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-3" class="forms__label forms__label-check forms__label-check-2 forange">Blue</label>
                                                        <input id="check-4" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-4" class="forms__label forms__label-check forms__label-check-2">Green</label>
                                                        <input id="check-5" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-5" class="forms__label forms__label-check forms__label-check-2">Yellow</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-8" class="btn-collapse"><i class="icon"></i> Size</a></div>
                                            <div id="categories-8" class="panel-collapse collapse">
                                                <div class="label-group">
                                                    <div class="checkbox-group">
                                                        <input id="check-1" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-1" class="forms__label forms__label-check forms__label-check-2 fblack">Black</label>
                                                        <input id="check-2" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-2" class="forms__label forms__label-check forms__label-check-2 fgrey8">White</label>
                                                        <input id="check-3" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-3" class="forms__label forms__label-check forms__label-check-2 forange">Blue</label>
                                                        <input id="check-4" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-4" class="forms__label forms__label-check forms__label-check-2">Green</label>
                                                        <input id="check-5" type="checkbox" class="forms__check hidden"/>
                                                        <label for="check-5" class="forms__label forms__label-check forms__label-check-2">Yellow</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </section>
                                {{--End sidebar category shop by--}}

                                <button class="btn-filter btn btn-primary btn-effect">FILTER</button>
                            </form>
                        </aside>
                        <!-- end .sidebar-->


                    </div>
                </div>
                {{--End Shop Content--}}
            </div>



        @include('includes.footer')
            <!-- end .footer-type-1-->


        </div>
        <!-- end layout-theme-->
    </div>


    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>
</html>
