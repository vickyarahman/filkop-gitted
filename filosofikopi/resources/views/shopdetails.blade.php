<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="" name="description"/>
        <meta content="" name="keywords"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta content="telephone=no" name="format-detection"/>
        <meta name="HandheldFriendly" content="true"/>

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

    </head>
    <body>
    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">
        @include('includes.headerwhite')


        <div class="wrap-content">

            {{--Breadcrumbs--}}
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            {{--<li><a href="home.html">Home</a></li>--}}
                            {{--<li><a href="home.html">Catalog</a></li>--}}
                            {{--<li><a href="home.html">Clothong</a></li>--}}
                            {{--<li class="active">raincoats</li>--}}
                        </ol>
                    </div>
                </div>
            </div>
            {{--End Breadcrumbs--}}

            {{--Start Carting--}}
            <div class="goods-card">
                <div class="container">
                    <div class="row" style="margin-left: -10%">
                        <div class="b-goods-carousel">
                            <div class="col-md-1">
                                <div id="bx-pager" class="b-goods-carousel__thumb">
                                    <a data-slide-index="0" href="" class="b-goods-carousel__thumb-link"><img src="images/productdetails/thumbnail/SdThumbFr01.jpg" alt="foto" class="b-goods-carousel__thumb-img"/></a>
                                    <a data-slide-index="1" href="" class="b-goods-carousel__thumb-link"><img src="images/productdetails/thumbnail/SdThumbFr03.jpg" alt="foto" class="b-goods-carousel__thumb-img"/></a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <ul class="b-goods-carousel__main-img bxslider">
                                    <li><img src="images/productdetails/main/SdBigFr01.jpg" alt="foto"/></li>

                                </ul>
                            </div>
                            <!-- end b-goods-carousel-->

                            <div class="col-lg-offset-1 col-lg-4 col-md-offset-1 col-md-4">
                                <section class="b-goods-3 b-goods-3_mod-a">
                                    <div class="b-goods-3__category">Filkop Ride</div>
                                    <h3 class="b-goods-3__name">sidi</h3>
                                    <div class="b-goods-3__price color-primary">Rp 160.000</div>
                                    {{--<div class="b-goods-3__price-old">Rp 190.000</div>--}}
                                    <div class="b-goods-3__label bg-secondary">Sale</div>
                                    <div class="b-goods-3__description">"Bagaimana Jika Ternyata Surga Tidak Menyenangkan atau Tidak Ada"</div>
                                </section>
                                <!-- end b-goods-->


                                <form class="form-goods form-goods_color_light form-horizontal">
                                    <div class="form-group">
                                        <label class="form-goods__label col-md-4">Color:</label>
                                        <div class="check-group col-md-8">
                                            <input id="check-1" type="checkbox" class="forms__check hidden">
                                            <label for="check-1" class="forms__label forms__label-check forms__label-check-3 forms__label-check-3_grey"></label>
                                            <input id="check-2" type="checkbox" checked class="forms__check hidden">
                                            <label for="check-2" class="forms__label forms__label-check forms__label-check-3 forms__label-check-3_blue"></label>
                                            <input id="check-3" type="checkbox" class="forms__check hidden">
                                            <label for="check-3" class="forms__label forms__label-check forms__label-check-3 forms__label-check-3_braun"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-goods__label col-md-4">Size:</label>
                                        <div class="col-md-8">
                                            <select data-width="auto" class="selectpicker">
                                                <option>Slimfit M</option>
                                                <option>Slimfit L</option>
                                                <option>XL</option>
                                                <option>XXL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="enumerator"><span class="enumerator__btn js-minus_btn">-</span>
                                        <input type="text" value="1" class="enumerator__input"><span class="enumerator__btn js-plus_btn">+</span>
                                    </div>
                                    <button class="btn btn-primary btn-effect"><a href="cart" class="fblack">Proceed</a></button>
                                    <footer class="form-goods__footer">
                                        {{--<div class="pull-left">--}}
                                            {{--<div class="form-goods__footer-title">Share:</div>--}}
                                            {{--<ul class="social-net list-inline">--}}
                                                {{--<li class="social-net__item"><a href="facebook.com" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>--}}
                                                {{--<li class="social-net__item"><a href="twitter.com" class="social-net__link"><i class="icon fa fa-twitter"></i></a></li>--}}
                                                {{--<li class="social-net__item"><a href="pinterest-p.com" class="social-net__link"><i class="icon fa fa-pinterest-p"></i></a></li>--}}
                                                {{--<li class="social-net__item"><a href="vk.com" class="social-net__link"><i class="icon fa fa-vk"></i></a></li>--}}
                                            {{--</ul>--}}
                                            {{--<!-- end social-list-->--}}
                                        {{--</div>--}}
                                        {{--<div class="b-goods-links pull-right"><a href="home-1.html" class="b-goods-links__item"><i class="icon fa fa-random"></i></a><a href="home-1.html" class="b-goods-links__item"><i class="icon fa fa-heart-o"></i></a></div>--}}
                                    </footer>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {{--End Carting--}}

            {{--Start Shop Details--}}
            <div class="b-goods-tab">
                <!-- Nav tabs-->
                <ul class="b-goods-tab-nav nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab">DESCRIPTION</a></li>
                    <li><a href="#information" data-toggle="tab">ADDITIONAL INFORMATION</a></li>
                    <li><a href="#reviews" data-toggle="tab">REVIEWS (2)</a></li>
                    <li><a href="#related" data-toggle="tab">RELATED PRODUCTS</a></li>
                </ul>
                <!-- Tab panes-->
                <div class="b-goods-tab-content tab-content">

                    {{--Description--}}
                    <div id="home" class="tab-pane fade in active">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2 text-center">
                                    <section class="section-goods-description">
                                        <h2 class="b-goods-tab-content__title">Sidi</h2>
                                        <div class="b-goods-tab-content__subtitle color-primary">Filosofi Kopi Ride</div>
                                        <div class="b-goods-tab-content__text">Bagaimana jika surga hanyalah harapan sehabis mati?<br>Bagaimana jika ternyata surga tidak menyenangkan?<br>Atau bagaimana jika ternyata surga tidak ada?</div>
                                        <img style="height: 150px;width: 150px" src="images/product/product1.jpg" alt="brand" class="b-goods-tab-content__brand-2 center-block img-responsive"/>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--End Description--}}

                    {{--Additional Information--}}
                    <div id="information" class="tab-pane fade">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-container">
                                        <table class="b-goods-table-information">
                                            <tbody>
                                            <tr>
                                                <td><i class="icon flaticon-signs"></i><i class="icon flaticon-signs-24"></i><i class="icon flaticon-signs-3"></i><i class="icon flaticon-signs-15"></i><i class="icon flaticon-signs-19"></i></td>
                                                <td>Fusce vestibulum justo id varius tristique. Vivamus purus odio, interdum id massa ullamcorper, tempus.</td>
                                            </tr>
                                            <tr>
                                                <td class="color-secondary">Material:</td>
                                                <td>Cotton</td>
                                            </tr>
                                            <tr>
                                                <td class="color-secondary">Weight:</td>
                                                <td>100 g</td>
                                            </tr>
                                            <tr>
                                                <td class="color-secondary">Color:</td>
                                                <td>Black</td>
                                            </tr>
                                            <tr>
                                                <td class="color-secondary">Sizes:</td>
                                                <td>Slimfit M, Slimfit L, XL, XXL</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--End Additional Information--}}

                    {{--Reviews--}}
                    <div id="reviews" class="tab-pane fade">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <section class="section-comment b-goods-reviews">
                                        <h2 class="b-goods-reviews__title ui-title-block-2">Buyer Reviews:</h2>
                                        <ul class="comments-list list-unstyled">
                                            <li>
                                                <article class="comment b-goods-reviews__comment clearfix">
                                                    <header class="comment-header clearfix">
                                                        <div class="pull-left">
                                                            <cite class="comment-author">Sidhi Pradipta</cite>
                                                            <time datetime="2012-10-27" class="comment-datetime">02 Apr 2018</time>
                                                        </div>
                                                        <ul class="rating-list list-inline pull-right">
                                                            <li class="fa fa-star color-primary"></li>
                                                            <li class="fa fa-star color-primary"></li>
                                                            <li class="fa fa-star color-primary"></li>
                                                            <li class="fa fa-star color-primary"></li>
                                                            <li class="fa fa-star color-primary"></li>
                                                        </ul>
                                                    </header>
                                                    <div class="comment-body">
                                                        <p>"Pas banget ama badan, bahan ga bikin gerah. Bikin appearance gue 'garang' abis. Nice,nice."</p>
                                                    </div>
                                                </article>
                                            </li>
                                            <li>
                                                <article class="comment b-goods-reviews__comment clearfix">
                                                    <header class="comment-header clearfix">
                                                        <div class="pull-left">
                                                            <cite class="comment-author">Mikołaj Kopernik</cite>
                                                            <time datetime="2012-10-27" class="comment-datetime">03 Apr 2018</time>
                                                        </div>
                                                        <ul class="rating-list list-inline pull-right">
                                                            <li class="fa fa-star color-primary"></li>
                                                            <li class="fa fa-star color-primary"></li>
                                                            <li class="fa fa-star color-primary"></li>
                                                            <li class="fa fa-star color-primary"></li>
                                                            <li class="fa fa-star-o"></li>
                                                        </ul>
                                                    </header>
                                                    <div class="comment-body">
                                                        <p>Proin non ultricies mauris, non varius massa. Maecenas tempus risus ut erat blandit fermentum. Praesent commodo quam non lacus interdum semper et ut enim. Donec vel suscipit nulla. Nullam imperdiet nisl in lectus porta sodales. Curabitur consequat dui nec eleifend tempor. Pellentesque elementum blandit interdum.</p>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </section>

                                </div>
                            </div>
                        </div>
                    </div>
                    {{--End Reviews--}}

                    <div id="related" class="tab-pane fade">
                        <div class="container">
                            <div class="tab-group-goods">
                                <div class="js-scroll-content">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket1.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket1.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket2.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket2.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket3.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket3.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket4.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket4.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket5.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket5.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket6.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket6.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket7.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket7.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket8.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket8.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                    </div>
                                </div><span class="btn-scroll-next btn btn-default btn-effect center-block js-scroll-next"><i class="icon fa fa-arrow-circle-down color-primary"></i>Load more</span>
                                <div class="js-scroll-content">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket1.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket1.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket2.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket2.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket3.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket3.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                        <div class="col-md-3">
                                            <section class="b-goods b-goods_mod-b">
                                                <div class="b-goods__inner"><a href="images/product/homemarket4.jpg" class="b-goods__img js-zoom-images"><img src="images/product/homemarket4.jpg" alt="goods" class="img-responsive"/></a>
                                                    <br><h3 class="b-goods__name">Epimetheus</h3>
                                                    <div class="b-goods__price">Rp 160.000</div><br>
                                                    <a href="shopdetails" class="b-goods-links__item b-goods-links__item_main">See More </a>
                                                </div>
                                            </section>
                                            <!-- end b-goods-->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--End Shop Details--}}


            @include('includes.footer')

        </div>
        <!-- end layout-theme-->
    </div>


    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    <!-- Slider-->
    <script src="assets/plugins/bxslider/vendor/jquery.easing.1.3.js"></script>
    <script src="assets/plugins/bxslider/vendor/jquery.fitvids.js"></script>
    <script src="assets/plugins/bxslider/jquery.bxslider.min.js"></script>
    </body>
</html>
