<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

    </head>
    <body>
    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">

        @include('includes.headerblack')

        <div class="wrap-content">

            <div class="container" style="padding-top: 5%">

                <div class="row">
                    <div class="col-md-12 ">


                        <div class="woocommerce">

                            <p class="woocommerce-thankyou-order-received">Thank you. Your order has been received.</p>

                            <ul class="woocommerce-thankyou-order-details order_details">
                                <li class="order">
                                    Order Number:				<strong>110418-0001</strong>
                                </li>
                                <li class="date">
                                    Date:				<strong>April 14, 2018</strong>
                                </li>
                                <li class="total">
                                    Total:				<strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120.000</span></strong>
                                </li>
                                <li class="method">
                                    Payment Method:				<strong>Virtual Payments</strong>
                                </li>
                            </ul>
                            <div class="clear"></div>

                            <div class="fbggrey7" style="padding: 2%; margin-bottom:2%;text-align: center">
                                <h5 class="fgrey3" style="padding-bottom: 10px">Please send with your code below :</h5>
                                <h4>DOKU Virtual Payment 88899826686</h4>
                                <h4>or</h4>
                                <h4>BCA - 12712345678 a/n Filosofi Kopi</h4>
                                <br>
                                <a href="howtopay"><h5>How to Pay</h5></a>

                            </div>
                            <div class="row">



                                <div class="col-lg-4">

                                    <h2>Order Details</h2>
                                    <table class="shop_table order_details">
                                        <thead>
                                        <tr>
                                            <th class="product-name">Product</th>
                                            <th class="product-total">Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="order_item">
                                            <td class="product-name">
                                                <a href="shopdetails">Filosofi Kopi Ride - Surga</a> <strong class="product-quantity">× 1</strong>	</td>
                                            <td class="product-total">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120.000</span>	</td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th scope="row">Subtotal:</th>
                                            <td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120.000</span></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Total:</th>
                                            <td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120.000</span></td>
                                        </tr>
                                        </tfoot>
                                    </table>




                                </div>

                                <div class="col-lg-4">

                                    <header><h2>Customer Details</h2></header>

                                    <table class="shop_table customer_details">
                                        <tbody><tr>
                                            <th>Note:</th>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <th>Email:</th>
                                            <td>vickyarahman@gmail.com</td>
                                        </tr>

                                        <tr>
                                            <th>Telephone:</th>
                                            <td>081321826686</td>
                                        </tr>

                                        </tbody></table>

                                </div>

                                <div class="col-lg-4">


                                    <header class="title">
                                        <h3>Billing Address</h3>
                                    </header>
                                    <address>
                                        Jl Pluto Utara II No 7<br>Bandung, 40286
                                    </address>

                                </div>     </div>


                        </div>


                    </div>


                </div>
            </div>

        </div>


        <!-- end layout-theme-->
        @include('includes.footer')
    </div>



    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>
</html>
