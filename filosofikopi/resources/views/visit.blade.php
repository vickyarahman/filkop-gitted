<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

    </head>
    <body>

    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">
        @include('includes.headerblack')

        <div class="wrap-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="home">Home</a></li>
                            <li class="active">Visit</li>
                        </ol>
                    </div>
                </div>
            </div>

            <!-- end b-title-page-->

            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="l-main-content">
                            <!-- Shop List-->
                            <div class="b-goods-catalog">
                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><img src="images/visits/visit1.jpg" alt="goods" class="img-responsive"/>
                                        <div class="b-goods__wrap">
                                            <div><h2 class="fontapercued tleft">Kedai Melawai</h2></div>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jalan Melawai I</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Kebayoran Lama</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta Selatan</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta,12330</h3>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Monday-Friday 8am - 7pm</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Saturday-Sunday 10am - 7pm</h3>
                                            <br><br>
                                            <a href="visitdetails" class="fwhite"><button class="btn btn-primary btn-effect fwhite" style="background-color: #0a0a0a;">Get Direction</button></a>
                                        </div>
                                    </div>
                                </section>

                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><img src="images/visits/visit1.jpg" alt="goods" class="img-responsive"/>
                                        <div class="b-goods__wrap">
                                            <div><h2 class="fontapercued tleft">Kedai Melawai</h2></div>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jalan Melawai I</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Kebayoran Lama</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta Selatan</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta,12330</h3>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Monday-Friday 8am - 7pm</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Saturday-Sunday 10am - 7pm</h3>
                                            <br><br>
                                            <a href="visitdetails" class="fwhite"><button class="btn btn-primary btn-effect fwhite" style="background-color: #0a0a0a;">Get Direction</button></a>
                                        </div>
                                    </div>
                                </section>

                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><img src="images/visits/visit1.jpg" alt="goods" class="img-responsive"/>
                                        <div class="b-goods__wrap">
                                            <div><h2 class="fontapercued tleft">Kedai Melawai</h2></div>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jalan Melawai I</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Kebayoran Lama</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta Selatan</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta,12330</h3>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Monday-Friday 8am - 7pm</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Saturday-Sunday 10am - 7pm</h3>
                                            <br><br>
                                            <button class="btn btn-primary btn-effect" style="background-color: #0a0a0a;">Get Direction</button>
                                        </div>
                                    </div>
                                </section>

                                <div class="visitspace"></div>

                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><img src="images/visits/visit1.jpg" alt="goods" class="img-responsive"/>
                                        <div class="b-goods__wrap">
                                            <div><h2 class="fontapercued tleft">Kedai Melawai</h2></div>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jalan Melawai I</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Kebayoran Lama</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta Selatan</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta,12330</h3>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Monday-Friday 8am - 7pm</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Saturday-Sunday 10am - 7pm</h3>
                                            <br><br>
                                            <a href="visitdetails" class="fwhite"><button class="btn btn-primary btn-effect fwhite" style="background-color: #0a0a0a;">Get Direction</button></a>
                                        </div>
                                    </div>
                                </section>

                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><img src="images/visits/visit1.jpg" alt="goods" class="img-responsive"/>
                                        <div class="b-goods__wrap">
                                            <div><h2 class="fontapercued tleft">Kedai Melawai</h2></div>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jalan Melawai I</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Kebayoran Lama</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta Selatan</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta,12330</h3>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Monday-Friday 8am - 7pm</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Saturday-Sunday 10am - 7pm</h3>
                                            <br><br>
                                            <a href="visitdetails" class="fwhite"><button class="btn btn-primary btn-effect fwhite" style="background-color: #0a0a0a;">Get Direction</button></a>
                                        </div>
                                    </div>
                                </section>

                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><img src="images/visits/visit1.jpg" alt="goods" class="img-responsive"/>
                                        <div class="b-goods__wrap">
                                            <div><h2 class="fontapercued tleft">Kedai Melawai</h2></div>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jalan Melawai I</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Kebayoran Lama</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta Selatan</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft">Jakarta,12330</h3>
                                            <br>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Monday-Friday 8am - 7pm</h3>
                                            <h3 class="b-goods__name fontapercued fgrey5 tleft"> Saturday-Sunday 10am - 7pm</h3>
                                            <br><br>
                                            <a href="visitdetails" class="fwhite"><button class="btn btn-primary btn-effect fwhite" style="background-color: #0a0a0a;">Get Direction</button></a>
                                        </div>
                                    </div>
                                </section>
                            </div>



                            <!-- End Shop List-->

                        </div>
                    </div>
                </div>
            </div>

            @include('includes.footer')
            </footer>
            <!-- end .footer-type-1-->


        </div>
        <!-- end layout-theme-->
    </div>


    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>
</html>
