<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

    </head>
    <body>

    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">

        @include('includes.headerblack')

        <div class="wrap-content">

            <div class="text-center">
                <ol class="breadcrumb">
                    <li><a href="home.html">Home</a></li>
                    <li><a href="home.html">Catalog</a></li>
                    <li><a href="home.html">Clothong</a></li>
                    <li class="active">raincoats</li>
                </ol>
            </div>
            <div class="b-title-page b-title-page_mrg-btn_sm">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="b-title-page__title shuffle">Gallery</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end b-title-page-->

            <div class="b-gallery-4 b-isotope">
                <ul class="b-isotope__filter list-inline">
                    <li><a href="" data-filter="*" class="current">SHOW ALL</a></li>
                    <li><a href="" data-filter=".photography">Filosofi Kopi Ride</a></li>
                    {{--<li><a href="" data-filter=".print">Filosofi Kopi x Muklay</a></li>--}}
                    {{--<li><a href="" data-filter=".video">Filosofi Kopi x Muklay</a></li>--}}
                    <li><a href="" data-filter=".design">Filosofi Kopi x Muklay</a></li>
                </ul>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="js-zoom-gallery grid">
                                <div class="b-isotope__grid">
                                    <div class="grid-sizer"></div>
                                        <a href="images/product/product5.jpg" class="b-gallery__item grid-item js-zoom-gallery__item video">
                                        <div class="b-gallery__inner"><img src="images/product/product8.jpg" alt="foto"/></div></a><a href="images/product/product8.jpg" class="b-gallery__item grid-item js-zoom-gallery__item print photography">
                                        <div class="b-gallery__inner"><img src="images/product/product6.jpg" alt="foto"/></div></a><a href="images/product/product6.jpg" class="b-gallery__item grid-item js-zoom-gallery__item print photography">
                                        <div class="b-gallery__inner"><img src="images/product/product7.jpg" alt="foto"/></div></a><a href="images/product/product7.jpg" class="b-gallery__item grid-item js-zoom-gallery__item print photography">
                                        <div class="b-gallery__inner"><img src="images/product/product5.jpg" alt="foto"/></div></a><a href="images/product/product5.jpg" class="b-gallery__item grid-item grid-item_wx2 js-zoom-gallery__item print photography">
                                        <div class="b-gallery__inner"><img src="images/product/product9.jpg" alt="foto"/></div></a><a href="images/product/product9.jpg" class="b-gallery__item grid-item js-zoom-gallery__item print photography">

                                        <div class="b-gallery__inner"><img src="images/product/homemarket1.jpg" alt="foto"/></div></a><a href="images/product/homemarket1.jpg" class="b-gallery__item grid-item js-zoom-gallery__item print design">
                                        <div class="b-gallery__inner"><img src="images/product/homemarket2.jpg" alt="foto"/></div></a><a href="images/product/homemarket2.jpg" class="b-gallery__item grid-item js-zoom-gallery__item print design">
                                        <div class="b-gallery__inner"><img src="images/product/homemarket3.jpg" alt="foto"/></div></a><a href="images/product/homemarket3.jpg" class="b-gallery__item grid-item js-zoom-gallery__item design">
                                        <div class="b-gallery__inner"><img src="images/product/homemarket4.jpg" alt="foto"/></div></a><a href="images/product/homemarket4.jpg" class="b-gallery__item grid-item js-zoom-gallery__item print design">
                                        <div class="b-gallery__inner"><img src="images/product/homemarket5.jpg" alt="foto"/></div></a><a href="images/product/homemarket5.jpg" class="b-gallery__item grid-item js-zoom-gallery__item print design">
                                        <div class="b-gallery__inner"><img src="images/product/homemarket6.jpg" alt="foto"/></div></a><a href="images/product/homemarket6.jpg" class="b-gallery__item grid-item js-zoom-gallery__item print design">
                                        <div class="b-gallery__inner"><img src="images/product/homemarket7.jpg" alt="foto"/></div><a href="images/product/homemarket7.jpg" class="b-gallery__item grid-item js-zoom-gallery__item print design"></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @include('includes.footer')
            <!-- end .footer-type-1-->


        </div>
        <!-- end layout-theme-->
    </div>


    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>
</html>
