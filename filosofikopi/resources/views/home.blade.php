<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

    </head>
    <body>
    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">

        @include('includes.header')

        <div class="wrap-content" style="margin-top: -20%">

            <!-- Begin main-slider-->
            <div id="main-slider" data-slider-width="1920px" data-slider-height="985px" data-slider-arrows="false" data-slider-buttons="false" class="main-slider slider-pro">
                <div class="sp-slides">
                    <!-- Slide 1-->
                    <div class="sp-slide"><img src="images/home/homeslider001.png" alt="slider" class="sp-image"/>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1">
                                    <div data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="400" data-show-delay="400" data-hide-delay="400" class="main-slider__label sp-layer"> </div>
                                    <h2 data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="400" data-show-delay="400" data-hide-delay="400" class="main-slider__title sp-layer fonttcopsed" style="padding-top: 25%">Filkop x Muklay</h2>
                                </div>
                                <button class="btn btn-primary btn-effect" style="margin-top:-5%;background-color: #f69835;width: 22.5%">S h o p</button>
                            </div>


                        </div>
                    </div>
                    <!-- Slide 2-->
                    <div class="sp-slide"><img src="images/home/newheader1.jpg" alt="slider" class="sp-image"/>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1">
                                    <div data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="400" data-show-delay="400" data-hide-delay="400" class="main-slider__label sp-layer"> </div>
                                    <h2 data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="400" data-show-delay="400" data-hide-delay="400" class="main-slider__title sp-layer fonttcopsed" style="padding-top: 25%">Ride With Filosofi Kopi</h2>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-effect" style="margin-top:-5%;background-color: #f69835;width: 22.5%">S h o p</button>
                        </div>
                    </div>

                    <!-- Slide 3-->
                    <div class="sp-slide"><img src="images/home/newheader2.jpg" alt="slider" class="sp-image" style="background-position: center"/>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1">
                                    <div data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="400" data-show-delay="400" data-hide-delay="400" class="main-slider__label sp-layer"> </div>
                                    <h2 data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="400" data-show-delay="400" data-hide-delay="400" class="main-slider__title sp-layer fonttcopsed" style="padding-top: 25%">Occasionwear</h2>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-effect" style="margin-top:-5%;background-color: #f69835;width: 22.5%">S h o p</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end main-slider-->

           
                <div class="col-md-12" style="text-align: center;margin-bottom: 20px;margin-top: 5%">
                    <h1 class="fgrey3 fonthapercued">SEE WHAT'S NEW</h1>
                    <i class="fa fa-2x fa-chevron-down color-fgrey5"></i>
                </div>

            <div class="section-area-home" style="margin-bottom: 5%">

                <div class="block-table block-table_padd_100">
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">
                                <div class="categorybox">
                                    <a href="shop"><img src="images/home/homecat7.jpg" /> </a>
                                    <div class="cbcentered fonthapercued" style="font-size: 30px"><b>SALE T-SHIRT</b></div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">
                                <div class="categorybox">
                                    <a href="shop"><img src="images/home/homecat2.jpg" /> </a>
                                    <div class="cbcentered fonthapercued" style="font-size: 30px"><b>COFFEE</b></div>
                                </div>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">
                                <div class="categorybox">
                                    <a href="shop"><img src="images/home/homecat301.jpg" /> </a>
                                    <div class="cbcentered fonthapercued" style="font-size: 30px"><b>TOTEBAGS</b></div>
                                </div>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                </div>

                <div class="block-table block-table_padd_100">
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">
                                <div class="categorybox">
                                    <a href="shop"><img src="images/home/homecat4.jpg" /> </a>
                                    <div class="cbcentered fonthapercued" style="font-size: 30px"><b>VISIT</b></div>
                                </div>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">
                                <div class="categorybox">
                                    <a href="shop"><img src="images/home/homecat5.jpg" /> </a>
                                    <div class="cbcentered fonthapercued" style="font-size: 30px"><b>ABOUT</b></div>
                                </div>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">
                                <div class="categorybox">
                                    <a href="shop"><img src="images/home/homecat6.jpg" /> </a>
                                    <div class="cbcentered fonthapercued" style="font-size: 30px"><b>LOOKBOOK</b></div>
                                </div>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                </div>

                <div class="block-table block-table_padd_100">
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">
                                <div class="categorybox">
                                    <a href="shop"><img src="images/home/homecat7.jpg" /> </a>
                                    <div class="cbcentered fonthapercued" style="font-size: 30px"><b>NEW COLLECTION</b></div>
                                </div>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">
                                <div class="categorybox">
                                    <a href="shop"><img src="images/home/homecat8.jpg" /> </a>
                                    <div class="cbcentered fonthapercued" style="font-size: 30px"><b>COFFEE SUBSCRIPTION</b></div>
                                </div>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                    <div class="block-table__cell col-md-4">
                        <div class="block-table__inner bg-grey-2 newcategory-bordered">
                            <section class="b-presentation b-presentation_sm">
                                <div class="categorybox">
                                    <a href="shop"><img src="images/home/homecat9.jpg" /> </a>
                                    <div class="cbcentered fonthapercued" style="font-size: 30px"><b>SHIPPING & RETURNS</b></div>
                                </div>
                            </section>
                            <!-- end b-presentation-->
                        </div>
                    </div>
                </div>

            </div>

                    <div class="row">
                        <div class="col-xs-12 homevideo-container">
                            <iframe src="https://www.youtube.com/embed/D9d3KjChEi8?list=PLkwqKG1JvcShcvbXsf9GA5cY5L04bp0Zu" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>




            <section class="section-area">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <h1 class="fgrey3 fonthapercued">Featured Product</h1>
                        <i class="fa fa-2x fa-chevron-down color-fgrey5"></i>
                    </div>
                </div>
                    <div class="block-table__cell col-md-12 text-center">
                        <div class="block-table__inner bg-white">
                            <div class="main-flex-box fonttcopsed">
                                <div class="flex-box-content">
                                    <div data-min480="3" data-min768="3" data-min992="3" data-min1200="3" data-pagination="true" data-navigation="false" data-auto-play="4000" data-stop-on-hover="true" class="goods-carousel owl-carousel owl-theme enable-owl-carousel js-zoom-gallery">
                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/product/product5.jpg" class="b-goods__img js-zoom-images"><img src="images/product/product5.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">FRD01</h3>
                                                <div class="b-goods__price">Rp 160.000</div>
                                                <div class="b-goods__label bg-primary fbgorange">Filkop Ride</div>
                                            </div>
                                        </section>

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/home/homemarket1.jpg" class="b-goods__img js-zoom-images"><img src="images/home/homemarket1.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">FxM01</h3>
                                                <div class="b-goods__price">Rp 160.000</div>
                                                <div class="b-goods__label bg-primary fbgorange">Filkop x Muklay</div>
                                            </div>
                                        </section>

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/home/homemarket2.jpg" class="b-goods__img js-zoom-images"><img src="images/home/homemarket2.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">FxM02</h3>
                                                <div class="b-goods__price">Rp 160.000</div>
                                                <div class="b-goods__label bg-primary fbgorange">Filkop x Muklay</div>
                                            </div>
                                        </section>

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/home/homemarket3.jpg" class="b-goods__img js-zoom-images"><img src="images/home/homemarket3.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">FxM03</h3>
                                                <div class="b-goods__price">Rp 160.000</div>
                                                <div class="b-goods__label bg-primary fbgorange">Filkop x Muklay</div>
                                            </div>
                                        </section>

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/home/homemarket4.jpg" class="b-goods__img js-zoom-images"><img src="images/home/homemarket4.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">FxM04</h3>
                                                <div class="b-goods__price">Rp 160.000</div>
                                                <div class="b-goods__label bg-primary fbgorange">Filkop x Muklay</div>
                                            </div>
                                        </section>

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/home/homemarket5.jpg" class="b-goods__img js-zoom-images"><img src="images/home/homemarket5.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">FxM05</h3>
                                                <div class="b-goods__price">Rp 160.000</div>
                                                <div class="b-goods__label bg-primary fbgorange">Filkop x Muklay</div>
                                            </div>
                                        </section>

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/home/homemarket6.jpg" class="b-goods__img js-zoom-images"><img src="images/home/homemarket6.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">FxM06</h3>
                                                <div class="b-goods__price">Rp 160.000</div>
                                                <div class="b-goods__label bg-primary fbgorange">Filkop x Muklay</div>
                                            </div>
                                        </section>

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/home/homemarket7.jpg" class="b-goods__img js-zoom-images"><img src="images/home/homemarket7.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">FxM07</h3>
                                                <div class="b-goods__price">Rp 160.000</div>
                                                <div class="b-goods__label bg-primary fbgorange">Filkop x Muklay</div>
                                            </div>
                                        </section>

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/home/homemarket8.jpg" class="b-goods__img js-zoom-images"><img src="images/home/homemarket8.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">FxM08</h3>
                                                <div class="b-goods__price">Rp 160.000</div>
                                                <div class="b-goods__label bg-primary fbgorange">Filkop x Muklay</div>
                                            </div>
                                        </section>

                                        <section class="b-goods">
                                            <div class="b-goods__inner"><a href="images/home/homemarket9.jpg" class="b-goods__img js-zoom-images"><img src="images/home/homemarket9.jpg" alt="goods" class="img-responsive"/></a>
                                                <br>
                                                <h3 class="b-goods__name">FxM09</h3>
                                                <div class="b-goods__price">Rp 160.000</div>
                                                <div class="b-goods__label bg-primary fbgorange">Filkop x Muklay</div>
                                            </div>
                                        </section>

                                    </div>
                                    {{--<button class="btn btn-primary btn-effect fonthapercued">See &nbsp; More</button>--}}
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            {{--<section class="section-area" style="margin-top: 5%;margin-bottom:5%;" >--}}
                {{--<div class="block-table__cell col-md-12 text-center" style="margin-top: -100px">--}}
                    {{--<div class="block-table__inner bg-white">--}}
                        {{--<div class="main-flex-box" style="background-color: #FF9944">--}}
                            {{--<div class="flex-box-content">--}}
                                {{--<div data-min480="1" data-min768="1" data-min992="1" data-min1200="1" data-pagination="true" data-navigation="false" data-auto-play="4000" data-stop-on-hover="true" class="goods-carousel owl-carousel owl-theme enable-owl-carousel js-zoom-gallery">--}}
                                    {{--<section class="b-goods">--}}
                                        {{--<h3 class="b-title-page__title shuffle fwhite">"Bla bla bla"</h3>--}}
                                        {{--<br>--}}
                                        {{--<br>--}}
                                    {{--</section>--}}

                                    {{--<section class="b-goods">--}}
                                        {{--<h3 class="b-title-page__title shuffle fwhite">"Sesuatu dimulai dengan passion"</h3>--}}
                                        {{--<br>--}}
                                        {{--<br>--}}
                                    {{--</section>--}}

                                    {{--<section class="b-goods">--}}
                                        {{--<h3 class="b-title-page__title shuffle fwhite">"Sesuatu dimulai dengan passion"</h3>--}}
                                        {{--<br>--}}
                                        {{--<br>--}}
                                    {{--</section>--}}

                                    {{--<section class="b-goods">--}}
                                        {{--<h3 class="b-title-page__title shuffle fwhite">"Sesuatu dimulai dengan passion"</h3>--}}
                                        {{--<br>--}}
                                        {{--<br>--}}
                                        {{--<button class="btn btn-primary btn-effect fblack" style="background-color: #f0efef;">Get Direction</button>--}}
                                    {{--</section>--}}

                                    {{--<section class="b-goods">--}}
                                        {{--<h3 class="b-title-page__title shuffle fwhite">"Sesuatu dimulai dengan passion"</h3>--}}
                                        {{--<br>--}}
                                        {{--<br>--}}

                                    {{--</section>--}}

                                {{--</div><br>--}}
                                {{--<button class="btn btn-primary btn-effect fblack" style="background-color: #f0efef;">See More</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</section>--}}


            {{--<section class="section-area">--}}
                {{--<div class="row" style="width: 100%">--}}
                    {{--<div class="col-md-12 pull-left">--}}
                        {{--<h1 class="fontthecticed fgrey1 pull-left" style="padding-left: 50px"> Hey low</h1>--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</section>--}}


            @include('includes.footer')

        </div>
        <!-- end layout-theme-->
    </div>


    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>
</html>
