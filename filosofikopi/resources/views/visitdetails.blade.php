<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

    </head>
    <body>

    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">

        @include('includes.headerwhite')

        <div class="wrap-content">

            <!-- Breadcrumbs Visit Details-->
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="home">Home</a></li>
                            <li><a href="visit">Visit</a></li>
                            <li class="active">Kedai Melawai</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- End Breadcrumbs Visit Details-->

            <div class="row">
                <div class="col-xs-12">
                    <div data-pagination="false" data-navigation="true" data-single-item="true" data-auto-play="10000" data-transition-style="fade" data-main-text-animation="true" data-after-init-delay="2000" data-after-move-delay="1000" data-stop-on-hover="true" class="b-gallery-description__media owl-carousel owl-theme owl-theme_mod-b enable-owl-carousel" style="max-height: 700px">
                        <img src="images/visits/visit1.jpg" alt="foto" class="img-responsive"/>
                        <img src="images/visits/visit2.jpg" alt="foto" class="img-responsive"/>
                        <img src="images/visits/visit3.jpg" alt="foto" class="img-responsive"/>
                        <img src="images/visits/visit4.jpg" alt="foto" class="img-responsive"/>
                    </div>
                </div>
            </div>

            <!-- Content Visit Details Top-->
            <div class="container">
                <div class="row" style="padding-bottom: 2.5%">
                    <div class="col-md-3">
                        <h1 class="tleft_nopadding">Kedai Melawai</h1>
                    </div>
                    <div class="col-md-7 pull-left">
                        <ul class="social-net list-inline" style="padding-top: 2.5%">
                            <li class="social-net__item"><a href="facebook.com" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>
                            <li class="social-net__item"><a href="twitter.com" class="social-net__link"><i class="icon fa fa-twitter"></i></a></li>
                            <li class="social-net__item"><a href="pinterest-p.com" class="social-net__link"><i class="icon fa fa-pinterest-p"></i></a></li>
                            <li class="social-net__item"><a href="vk.com" class="social-net__link"><i class="icon fa fa-vk"></i></a></li>
                            <li class="social-net__item"><a href="vine.com" class="social-net__link"><i class="icon fa fa-vine"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 pull-right">
                        <button class="btn btn-primary btn-effect" style="background-color: #0a0a0a;">Get Direction</button>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="b-gallery-description__inner">
                            <h4 class="tleft_nopadding">Location Details</h4>
                            <p>Jalan Melawai 6 Blok M no 8, <br>
                                Kebayoran Baru, Melawai,<br>
                                Jakarta Selatan, Jakarta 12160</p>
                            <p>Coffee Bar: <br>
                                Monday - Saturday 7am - 3pm <br>
                                Sunday 8am - 3pm <br>
                            </p>

                            <p>Retail Store: <br>
                                Monday - Saturday 7am - 3pm <br>
                                Sunday 8am - 3pm <br>
                            </p>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="b-info">
                            <hr class="style1">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="visitdetail__name">Serving Hours</div>
                                </div>
                                <div class="col-md-6 pull-left">
                                        <div class="visitdetail__info">Breakfast 7am - 10.30am</div>
                                        <div class="visitdetail__info">Lunch 12pm - 6pm</div>
                                        <div class="visitdetail__info">Dinner 6pm - 12am</div>
                                        <div class="visitdetail__info">Room Service 24/7</div>
                                </div>
                            </div>
                            <hr class="style1">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="visitdetail__name">Address</div>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <div class="visitdetail__info">Jl. Melawai 1 no 12</div>
                                    <div class="visitdetail__info">Kebayoran Baru , Jakarta Selatan</div>
                                    <div class="visitdetail__info">Jakarta,12330</div>
                                    <div class="visitdetail__info">Room Service 24/7</div>
                                </div>
                            </div>
                            <hr class="style1">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="visitdetail__name">Social Media</div>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <ul class="social-net list-inline" style="margin-left: -20%">
                                        <li class="social-net__item"><a href="facebook.com" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>
                                        <li class="social-net__item"><a href="twitter.com" class="social-net__link"><i class="icon fa fa-twitter"></i></a></li>
                                        <li class="social-net__item"><a href="pinterest-p.com" class="social-net__link"><i class="icon fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Content Visit Details Top-->

            <div id="map" class="visitdetail__ui-map"></div>

            <!-- Content Visit Details Barista-->
            <div class="container" style="padding-top: 5%">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="l-main-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="">Barista Behind The Bars</h1>
                                </div>
                            </div>
                            <!-- Shop List-->
                            <div class="b-goods-catalog">
                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><img src="images/barista/cel.png" alt="goods" class="img-responsive"/>
                                        <div class="b-goods__wrap">
                                            <div><h2 class="fontapercued tleft">Celica Manda</h2></div>
                                            <br>
                                        </div>
                                    </div>
                                </section>

                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><img src="images/barista/ind.png" alt="goods" class="img-responsive" style="max-height: 80%"/>
                                        <div class="b-goods__wrap">
                                            <div><h2 class="fontapercued tleft">Indra Dwi Guna</h2></div>
                                            <br>
                                        </div>
                                    </div>
                                </section>

                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><img src="images/barista/kuh.png" alt="goods" class="img-responsive"/>
                                        <div class="b-goods__wrap">
                                            <div><h2 class="fontapercued tleft">Kukuh Dwi Satrio</h2></div>
                                            <br>
                                        </div>
                                    </div>
                                </section>

                                <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner"><img src="images/barista/namia.jpg" alt="goods" class="img-responsive"/>
                                        <div class="b-goods__wrap">
                                            <div><h2 class="fontapercued tleft">Namira</h2></div>
                                            <br>
                                        </div>
                                    </div>
                                </section>



                            </div>



                            <!-- End Shop List-->

                        </div>
                    </div>
                </div>
            </div>
            <!-- End Content Visit Details Barista-->

            <!-- end b-form-newsletter-->

            <!-- end .footer-type-1-->


        </div>
    @include('includes.footer')
        <!-- end layout-theme-->
    </div>


    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    <!-- User map-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEoGJnmhn0ZwVMevQwrHT4SLdWLZNUQz4"></script>
    <!-- Maps customization-->
    <script src="js/custom-map.js"></script>
    </body>
</html>
