<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{  backpack_url("elfinder") }}"><i class="fa fa-files-o"></i> <span>File manager</span></a></li>


<li class="treeview">
    <a href="#"><i class="fa fa-tag"></i> <span>Manage Home</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/homeheader') }}"><span>Header</span></a></li>
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/home') }}"><span>What's New</span></a></li>
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/home') }}"><span>Youtube</span></a></li>
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/home') }}"><span>New Collection</span></a></li>
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/home') }}"><span>Quote</span></a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-list"></i> <span>Menus</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/menu-item') }}"><span>Menu Item</span></a>
        </li>
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/permission') }}"><span>Permissions</span></a>
        </li>
    </ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-key"></i> <span>Roles & Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/role') }}"><span>Roles</span></a>
        </li>
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/permission') }}"><span>Permissions</span></a>
        </li>
    </ul>
</li>