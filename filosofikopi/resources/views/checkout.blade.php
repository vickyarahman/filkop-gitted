<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Filosofi Kopi</title>

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link rel="stylesheet" href="fonts/font_filkop_apercu/style.css" type="text/css"/>
        <link rel="stylesheet" href="fonts/font_filkop_copse/style.css" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="css/master.css"/>
        <link rel="stylesheet" href="css/theme.css"/>
        <link rel="stylesheet" href="css/color.css"/>
        <link rel="stylesheet" href="css/woo.css"/>
        <link rel="stylesheet" href="css/woocommerce-layout.css"/>
        <link rel="stylesheet" href="css/responsive.css"/>

    </head>
    <body>
    <div class="screen-loader">
        <div class="loading">
        <span class="loader_span">
            <span class="loader_right"></span>
            <span class="loader_left"></span>
        </span>
        </div>
        <div class="sl-top"></div>
        <div class="sl-bottom"></div>
    </div>
    <!-- Loader end-->

    <div  class="l-theme">

        @include('includes.headerblack')

        <div class="wrap-content">

            <div class="container" style="padding-top: 5%;">
                <div class="row">
                    <div class="col-md-9 ">
                        <div class="woocommerce">

                            <form class="checkout_coupon" method="post" style="display:none">
                                <p class="form-row form-row-first">
                                    <input type="text" name="coupon_code" class="input-text" placeholder="Coupon code" id="coupon_code" value="">
                                </p>
                                <p class="form-row form-row-last">
                                    <input type="submit" class="button" name="apply_coupon" value="Apply Coupon">
                                </p>
                                <div class="clear"></div>
                            </form>
                            <form name="checkout" method="post" class="checkout woocommerce-checkout" action="checkout/" enctype="multipart/form-data" _lpchecked="1">
                                <div class="col2-set" id="customer_details">
                                    <div class="col-1">
                                        <div class="woocommerce-billing-fields">
                                            <h3>Billing Details</h3>
                                            <p class="form-row form-row form-row-first ">
                                                <label>First Name <abbr class="required" title="required">*</abbr></label>
                                                <input type="text" class="input-text " name="billing_first_name" >
                                            </p>
                                            <p class="form-row form-row form-row-last " >
                                                <label>Last Name <abbr class="required" title="required">*</abbr></label>
                                                <input type="text" class="input-text "  >
                                            </p>
                                            <div class="clear"></div>
                                            <p class="form-row form-row form-row-wide" >
                                                <label for="billing_company" >Company Name</label>
                                                <input type="text" class="input-text " >
                                            </p>
                                            <p class="form-row form-row form-row-first" >
                                                <label>Email Address <abbr class="required" title="required">*</abbr></label>
                                                <input type="email" class="input-text ">
                                            </p>
                                            <p class="form-row form-row form-row-last" >
                                                <label>Phone <abbr class="required" title="required">*</abbr></label>
                                                <input type="tel" class="input-text ">
                                            </p>
                                            <div class="clear"></div>
                                            <p class="form-row form-row form-row-wide" >
                                                <label>Address <abbr class="required" title="required">*</abbr></label>
                                                <input type="text" class="input-text">
                                            </p>
                                            <p class="form-row form-row form-row-wide address-field">
                                                <input type="text" class="input-text">
                                            </p>



                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="woocommerce-shipping-fields">
                                            <h3>Additional Information</h3>
                                            <p class="form-row form-row" >
                                                <label for="order_comments" class="">Order Notes</label>
                                                <textarea name="order_comments" class="input-text " id="order_comments" placeholder="Notes about your order, e.g. special notes for delivery." ></textarea>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <h3 id="order_review_heading">Your order</h3>
                                <div id="order_review" class="woocommerce-checkout-review-order">
                                    <table class="shop_table woocommerce-checkout-review-order-table">
                                        <thead>
                                        <tr>
                                            <th class="product-name">Product</th>
                                            <th class="product-total">Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="cart_item">
                                            <td class="product-name"> Filosofi Kopi - Surga &nbsp; <strong class="product-quantity">× 1</strong></td>
                                            <td class="product-total"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120,000</span></td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr class="cart-subtotal">
                                            <th>Subtotal</th>
                                            <td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120,000</span></td>
                                        </tr>
                                        <tr class="order-total">
                                            <th>Total</th>
                                            <td><strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>120,000</span></strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <div id="payment" class="woocommerce-checkout-payment">
                                        <ul class="wc_payment_methods payment_methods methods">
                                            {{--<li class="wc_payment_method payment_method_cheque">--}}
                                                {{--<input  type="radio" class="input-radio">--}}
                                                {{--<label for="payment_method_cheque"> Payments Method Virtual Account</label>--}}
                                            {{--</li>--}}
                                            {{--<li class="wc_payment_method payment_method_cheque">--}}
                                                {{--<input  type="radio" class="input-radio">--}}
                                                {{--<label for="payment_method_cheque"> Payments Method Virtual Account</label>--}}
                                            {{--</li>--}}
                                        </ul>
                                        <div class="form-row place-order">
                                            <noscript>
                                                Since your browser does not support JavaScript, or it is disabled, please ensure you click the &lt;em&gt;Update Totals&lt;/em&gt; button before placing your order. You may be charged more than the amount stated above if you fail to do so.			&lt;br/&gt;&lt;input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" /&gt;
                                            </noscript>

                                            <button class="btn btn-primary btn-effect fonthapercued" style="background-color: #353535;min-width: 75%;margin-left: 12.5%"><a href="payment" class="fgrey6">P r o c e e d &nbsp;&nbsp;&nbsp;  P a y m e n t </a></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <aside class="l-sidebar">
                            <form class="form-filter">
                                <section class="section-sidebar">
                                    <h3 class="sidebar-title">categories</h3>
                                    <div id="accordion" class="accordion accordion-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-1" class="btn-collapse"><i class="icon"></i> Men</a></div>
                                            <div id="categories-1" class="panel-collapse collapse in">
                                                <div class="label-group">
                                                    <input id="categories__radio-1" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-1" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Apparel</a></label>
                                                    <input id="categories__radio-2" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-2" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Hats</a></label>
                                                    <input id="categories__radio-3" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-3" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Outer</a></label>
                                                    <input id="categories__radio-4" type="radio" name="categories-group-1" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-4" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Bag</a></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-2" class="btn-collapse"><i class="icon"></i> Women</a></div>
                                            <div id="categories-2" class="panel-collapse collapse">
                                                <div class="label-group">
                                                    <input id="categories__radio-5" type="radio" name="categories-group-2" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-5" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Apparel</a></label>
                                                    <input id="categories__radio-6" type="radio" name="categories-group-2" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-6" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Hats</a></label>
                                                    <input id="categories__radio-7" type="radio" name="categories-group-2" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-7" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Outer</a></label>
                                                    <input id="categories__radio-8" type="radio" name="categories-group-2" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-8" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Bag</a></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-3" class="btn-collapse"><i class="icon"></i> Coffee</a></div>
                                            <div id="categories-3" class="panel-collapse collapse">
                                                <div class="label-group">
                                                    <input id="categories__radio-5" type="radio" name="categories-group-3" value="" class="forms__radio hidden"/>
                                                    <label for="categories__radio-5" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Coffee</a></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-4" class="btn-collapse"><i class="icon"></i> Equipment</a></div>
                                            <div id="categories-4" class="panel-collapse collapse">
                                                <div class="label-group">
                                                    <label for="categories__radio-5" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Manual Brewing</a></label>
                                                    <input id="categories__radio-5" type="radio" name="categories-group-4" value="" class="forms__radio hidden"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#categories-5" class="btn-collapse"><i class="icon"></i> Sales</a></div>
                                            <div id="categories-5" class="panel-collapse collapse">
                                                <label for="categories__radio-5" class="forms__label forms__label-radio forms__label-radio-1"><a href="shop">Filosofi Kopi - Ride</a></label>
                                                <input id="categories__radio-5" type="radio" name="categories-group-5" value="" class="forms__radio hidden"/>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </form>
                        </aside>
                        <!-- end .sidebar-->

                    </div>
                </div>
            </div>

        </div>


        <!-- end layout-theme-->
        @include('includes.footer')
    </div>



    <!-- ++++++++++++-->
    <!-- MAIN SCRIPTS-->
    <!-- ++++++++++++-->
    <script src="libs/jquery-1.12.4.min.js"></script>
    <script src="libs/jquery-migrate-1.2.1.js"></script>
    <!-- Bootstrap-->
    <script src="libs/bootstrap/bootstrap.min.js"></script>
    <!-- User customization-->
    <script src="js/custom.js"></script>
    <!-- Other slider-->
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- Pop-up window-->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Headers scripts-->
    <script src="plugins/headers/slidebar.js"></script>
    <script src="plugins/headers/header.js"></script>
    <!-- Select customization-->
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <!-- Mail scripts-->
    <script src="plugins/jqBootstrapValidation.js"></script>
    <script src="plugins/contact_me.js"></script>
    <!-- Filter and sorting images-->
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <script src="plugins/isotope/imagesLoaded.js"></script>
    <!-- Shuffle-->
    <script src="plugins/letters/jquery.shuffleLetters.js"></script>
    <!-- Progress numbers-->
    <script src="plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
    <!-- Animations-->
    <script src="plugins/scrollreveal/scrollreveal.min.js"></script>
    <!-- Main slider-->
    <script src="plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>
</html>
